import SwaggerUI from 'swagger-ui'

/* eslint-disable
  typescript/no-var-requires
 */
// Fill body element
const body = require( './index.html' )
const specification = require( '../../../api/swagger/swagger.yaml' )
/* eslint-enable
  typescript/no-var-requires
 */

/* eslint-disable
  no-unsanitized/property
 */

const bundle = document.body.innerHTML
document.body.innerHTML = body + bundle

/* eslint-enable
  no-unsanitized/property
 */

SwaggerUI( {
  dom_id                  : '#root',
  layout                  : 'BaseLayout',
  docExpansion            : 'full',
  defaultModelsExpandDepth: 15,
  defaultModelExpandDepth : 15,
  url                     : specification
} )
