"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const webpack_merge_1 = __importDefault(require("webpack-merge"));
const webpack_1 = __importDefault(require("webpack"));
const webpack_common_1 = __importDefault(require("./webpack.common"));
const webpack_constant_1 = __importDefault(require("./webpack.constant"));
const { HotModuleReplacementPlugin } = webpack_1.default;
exports.default = webpack_merge_1.default(webpack_common_1.default.container, {
    mode: 'development',
    plugins: [new HotModuleReplacementPlugin()],
    devServer: {
        contentBase: webpack_constant_1.default.target.dist,
        port: 8000,
        historyApiFallback: true,
        inline: true,
        open: false
    }
});
//# sourceMappingURL=webpack.development.js.map