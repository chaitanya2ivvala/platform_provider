"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const database = { domain: 'https://tunnel.vultus.io/' };
const routes = {
    feature: {
        post: {
            request: ({ valid }) => {
                return {
                    code: valid ? 202 : 400,
                    message: `service ${valid ? 'accepted' : 'rejected'} feature`
                };
            },
            response: ({ valid }) => {
                return {
                    code: valid ? 202 : 400,
                    message: valid
                        ? 'service completed operation'
                        : 'service encountered an unexpected condition, operation terminated'
                };
            }
        }
    }
};
const messages = [
    [
        'general',
        {
            success: {
                code: 200,
                message: 'service completed operation'
            },
            error: {
                code: 500,
                message: 'service encountered an unexpected condition, operation terminated'
            }
        }
    ]
];
const errors = [
    [
        'feature-register-syntax',
        {
            params: { type: 'syntax error' },
            message: 'feature must pass validation'
        }
    ]
];
exports.default = {
    database,
    messages,
    routes,
    errors
};
//# sourceMappingURL=variables.js.map