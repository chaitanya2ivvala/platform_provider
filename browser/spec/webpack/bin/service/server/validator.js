"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const ajv_1 = __importDefault(require("ajv"));
const ajv_merge_patch_1 = __importDefault(require("ajv-merge-patch"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const js_yaml_1 = __importDefault(require("js-yaml"));
const lodash_1 = require("lodash");
require("../declaration/import");
const routes_1 = require("./routes");
const feature_1 = __importDefault(require("./elements/feature"));
const status_1 = __importStar(require("./elements/status"));
const unity_old_1 = require("../toolbox/unity.old");
class Validator extends ajv_1.default {
    constructor() {
        super({ allErrors: true });
        ajv_merge_patch_1.default(this);
        const { include } = Validator;
        this.addSchema(include('resources/feature'));
        this.addSchema(include('resources/feature/properties'));
        this.addSchema(include('resources/status'), 'resources-status');
        this.setup(routes_1.endpoints);
    }
    static get Instance() {
        return this._instance || (this._instance = new this());
    }
    static include(route, _env = {}) {
        const env = lodash_1.merge({
            name: 'routes',
            type: 'schema',
            format: 'yml'
        }, _env);
        const worker = (({ join }) => join)(path_1.default);
        const resources = {
            assembler: lodash_1.partial(function (res) {
                const { product, entry, site, article } = this;
                const w = lodash_1.partial((_res, func) => {
                    return func(_res);
                }, res);
                return product(...lodash_1.map([
                    entry, site, article
                ], w));
            }),
            product: lodash_1.partial((w, entry, site, article) => {
                return w(entry, site, article);
            }, worker),
            entry: lodash_1.partial((w, { name }) => {
                const prefix = __dirname;
                return w(prefix, name);
            }, worker),
            site: lodash_1.partial(({ route }) => {
                return route;
            }),
            article: lodash_1.partial((del, { type, format }) => {
                return lodash_1.join([type, format], del);
            }, '.')
        };
        const { assembler } = resources;
        const construct = lodash_1.bind(assembler, resources);
        const target = construct(Object.assign({ route }, env));
        try {
            return js_yaml_1.default.safeLoad(fs_1.default.readFileSync(target, 'utf8'));
        }
        catch (error) {
            return error;
        }
    }
    static accept(data) {
        return unity_old_1.norm(lodash_1.defaultTo(lodash_1.at(data, 'status.details.valid'), false));
    }
    setup(_endpoints) {
        const { include } = Validator;
        const resources = {
            assembler: lodash_1.partial(function (elements) {
                const { product, label, route } = this;
                return lodash_1.map(elements, (element) => {
                    const w = lodash_1.partial((_list, func) => {
                        return func(_list);
                    }, element);
                    return product(lodash_1.map([label, route], w));
                });
            }),
            product: lodash_1.partial((keys, values) => {
                return lodash_1.fromPairs(lodash_1.zip(keys, values));
            }, ['label', 'route']),
            label: lodash_1.partial((del, { list }) => {
                return lodash_1.join(list, del);
            }, '-'),
            route: lodash_1.partial((del, { list }) => {
                return lodash_1.join(list, del);
            }, '/')
        };
        const { assembler } = resources;
        const construct = lodash_1.bind(assembler, resources);
        const index = construct(_endpoints);
        const append = lodash_1.partial((target, { label, route }) => {
            const file = include(route);
            target.addSchema(file, label);
            return lodash_1.isEqual(lodash_1.isError(file), false);
        }, this);
        return lodash_1.map(index, append);
    }
    check(data, label) {
        const valid = this.validate(label, data);
        const { errors } = this;
        return {
            valid,
            errors,
            errorCount: valid ? 0 : lodash_1.size(errors)
        };
    }
    assamble(data, label) {
        const details = this.check(data, label);
        const partialStatus = status_1.default(details, label);
        const partialFeature = feature_1.default(data);
        const fullStatus = { status: status_1.updateStatus(data, unity_old_1.norm(lodash_1.at(partialStatus, 'status'))) };
        const mergeFeature = ({ feature, _id }, { status }) => {
            return {
                _id,
                status,
                feature
            };
        };
        return mergeFeature(partialFeature, fullStatus);
    }
}
exports.Validator = Validator;
exports.assets = {
    include: Validator.include,
    accept: Validator.accept
};
exports.default = Validator.Instance;
//# sourceMappingURL=validator.js.map