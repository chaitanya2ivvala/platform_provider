"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const body_parser_1 = __importDefault(require("body-parser"));
const express_1 = __importDefault(require("express"));
require("../declaration/import");
const logger_1 = __importDefault(require("../toolbox/logger"));
const validator_1 = __importDefault(require("./validator"));
class Server extends validator_1.default {
    constructor(port = 3000) {
        super();
        this.app = express_1.default();
        const { app } = this;
        app.use(body_parser_1.default.json());
        this.app.use((req, _res, next) => {
            if (req.method === 'POST') {
                req.body.createdAt = Date.now();
                req.body.modifiedAt = Date.now();
            }
            next();
        });
        app.post('/feature', (req, res) => {
            const validator = new validator_1.default();
            const product = validator.assamble(req.body, 'feature-post-request');
            res.send(product);
        });
        app.listen(port, () => {
            const { info } = logger_1.default;
            info('Express is running');
        });
    }
}
exports.Server = Server;
exports.default = {};
//# sourceMappingURL=interface.js.map