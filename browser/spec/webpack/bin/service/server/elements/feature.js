"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
const base64url_1 = __importDefault(require("base64url"));
const unity_old_1 = require("../../toolbox/unity.old");
const logger_1 = __importDefault(require("../../toolbox/logger"));
const assembler = ({ _id, feature }) => {
    const fromBase = (v) => base64url_1.default.decode(v);
    const fromText = (v) => base64url_1.default(v);
    const referencePath = 'properties.item.reference';
    const reference = [[fromText, unity_old_1.norm(lodash_1.at(feature, referencePath))], [fromBase, unity_old_1.norm(_id)]];
    const suitable = lodash_1.map(lodash_1.map(reference, lodash_1.compact), (pair) => lodash_1.fromPairs(lodash_1.zip(['func', 'value'], pair)));
    const processed = lodash_1.map(suitable, ({ func, value }) => {
        if (lodash_1.isUndefined(value)) {
            return [];
        }
        return [value, func(value)];
    });
    const diff = lodash_1.difference(lodash_1.head(processed), lodash_1.reverse(lodash_1.last(processed)));
    const { warn } = logger_1.default;
    let master = lodash_1.last(processed);
    if (lodash_1.isEmpty(master)) {
        if (lodash_1.isEqual(lodash_1.size(diff), 2)) {
            master = diff;
        }
        else {
            warn('Feature contain to ID or reference.');
        }
    }
    lodash_1.set(feature, referencePath, lodash_1.head(master));
    const result = {
        _id: lodash_1.last(master),
        feature
    };
    return result;
};
exports.default = assembler;
//# sourceMappingURL=feature.js.map