"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
const unity_old_1 = require("../../toolbox/unity.old");
const validator_1 = __importDefault(require("../validator"));
const variables_1 = __importDefault(require("../variables"));
const route = lodash_1.partial((del, path) => {
    return lodash_1.join(path, del);
}, '.');
const path = lodash_1.partial((del, label) => {
    return lodash_1.split(label, del);
}, '-');
exports.default = (details, label) => {
    const { routes } = variables_1.default;
    const provider = lodash_1.at(routes, route(path(unity_old_1.norm(label))));
    const status = lodash_1.merge({}, ...lodash_1.map(provider, (func) => func(details)), { details });
    return { status };
};
exports.elements = {
    details: (errors) => {
        return {
            valid: lodash_1.isEmpty(errors),
            errors,
            errorCount: lodash_1.size(errors)
        };
    },
    status: (errors, _env = null) => {
        const env = {
            success: {
                code: 200,
                message: 'service completed operation'
            },
            error: {
                code: 500,
                message: 'service encountered an unexpected condition, operation terminated'
            }
        };
        if (lodash_1.isEqual(lodash_1.isNull(_env), false)) {
            lodash_1.merge(env, _env);
        }
        const partDetails = (({ details }) => {
            return details(errors);
        })(exports.elements);
        const partialStatus = (({ valid }, { success, error }) => {
            return valid ? success : error;
        })(partDetails, env);
        return (({ code, message }, details) => {
            return {
                code,
                message,
                details
            };
        })(partialStatus, partDetails);
    }
};
exports.updateStatus = ({ status }, child) => {
    const extract = ({ details }) => {
        const { errors } = details;
        return lodash_1.isNull(errors) ? [] : errors;
    };
    const suitable = (source) => {
        const { valid } = validator_1.default.check(source, 'resources-status');
        return valid ? source : false;
    };
    const stack = lodash_1.compact([suitable(status), suitable(child)]);
    const fullErrors = lodash_1.flatMap(stack, extract);
    const partialMessage = ({ code, message }) => {
        const index = {
            200: 'success',
            400: 'error',
            500: 'error'
        };
        return [
            index[lodash_1.round(code, -2)],
            {
                code,
                message
            }
        ];
    };
    const fullMessage = lodash_1.fromPairs(lodash_1.map(stack, partialMessage));
    const fullStatus = ((errors, message) => {
        const func = exports.elements.status;
        return func(errors, message);
    })(fullErrors, fullMessage);
    return fullStatus;
};
//# sourceMappingURL=status.js.map