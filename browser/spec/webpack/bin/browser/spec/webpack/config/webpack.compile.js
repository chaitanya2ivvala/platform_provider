"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const webpack_constant_1 = __importDefault(require("./webpack.constant"));
var Compile;
(function (Compile) {
    Compile.rules = [
        {
            enforce: 'pre',
            test: /\.js$/,
            loaders: ['source-map-loader'],
            exclude: webpack_constant_1.default.structure.modules.path
        },
        {
            enforce: 'post',
            test: /\.tsx?$/,
            loader: 'awesome-typescript-loader',
            options: { configFileName: webpack_constant_1.default.target.tsconfig }
        },
        {
            enforce: 'post',
            test: /\.html$/,
            exclude: webpack_constant_1.default.structure.modules.path,
            loader: 'html-loader'
        },
        {
            test: /\.css$/,
            use: ['style-loader', 'css-loader']
        },
        {
            test: /\.yaml$/,
            include: [webpack_constant_1.default.target.project],
            use: ['file-loader?name=[name].json', 'yaml-loader']
        },
        {
            test: /\.(eot|svg|ttf|woff|woff2)$/,
            include: [webpack_constant_1.default.structure.modules.path],
            loader: 'file-loader?name=[name].[ext]'
        },
        {
            test: /\.jpe?g$|\.ico$|\.gif$|\.png$|\.svg$|\.wav$|\.mp3$|\.mp4$/,
            include: [webpack_constant_1.default.structure.modules.path],
            loader: 'file-loader?name=[name].[ext]'
        },
        {
            enforce: 'post',
            test: /\.(jpe?g|png|gif|svg)$/i,
            exclude: webpack_constant_1.default.structure.modules.path,
            use: [
                {
                    loader: 'url-loader',
                    options: {
                        limit: 1024 * 128,
                        mimetype: 'image/png'
                    }
                }
            ]
        }
    ];
    Compile.extensions = [
        '.ts',
        '.js',
        '.html',
        '.json'
    ];
})(Compile || (Compile = {}));
exports.default = Compile;
//# sourceMappingURL=webpack.compile.js.map