"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const favicons_webpack_plugin_1 = __importDefault(require("favicons-webpack-plugin"));
const html_webpack_plugin_1 = __importDefault(require("html-webpack-plugin"));
const path_1 = __importDefault(require("path"));
const webpack_constant_1 = __importDefault(require("./webpack.constant"));
var Bootstrap;
(function (Bootstrap) {
    Bootstrap.template = new html_webpack_plugin_1.default({
        title: webpack_constant_1.default.application.title,
        filename: webpack_constant_1.default.application.entry
    });
    Bootstrap.favicon = new favicons_webpack_plugin_1.default({
        logo: path_1.default.resolve(webpack_constant_1.default.target.resources, 'images', 'favicon', 'vultus_ab_icon.png'),
        prefix: 'favicon/',
        icons: {
            android: false,
            appleIcon: false,
            appleStartup: false,
            coast: false,
            favicons: true,
            firefox: true,
            opengraph: false,
            twitter: false,
            yandex: false,
            windows: false
        }
    });
})(Bootstrap = exports.Bootstrap || (exports.Bootstrap = {}));
exports.default = Bootstrap;
//# sourceMappingURL=webpack.bootstrap.js.map