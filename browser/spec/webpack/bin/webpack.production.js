"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const compression_webpack_plugin_1 = __importDefault(require("compression-webpack-plugin"));
const uglifyjs_webpack_plugin_1 = __importDefault(require("uglifyjs-webpack-plugin"));
const webpack_merge_1 = __importDefault(require("webpack-merge"));
const webpack_1 = __importDefault(require("webpack"));
const webpack_common_1 = __importDefault(require("./webpack.common"));
const { DefinePlugin } = webpack_1.default;
exports.default = webpack_merge_1.default(webpack_common_1.default.container, {
    mode: 'production',
    stats: { warnings: false },
    plugins: [
        new uglifyjs_webpack_plugin_1.default({ sourceMap: false }),
        new DefinePlugin({ 'process.env.NODE_ENV': JSON.stringify('production') }),
        new compression_webpack_plugin_1.default({
            asset: '[path].gz[query]',
            algorithm: 'gzip',
            test: /\.js$|\.ts$|\.css$|\.html$/,
            threshold: 10240,
            minRatio: 0.8
        })
    ]
});
//# sourceMappingURL=webpack.production.js.map