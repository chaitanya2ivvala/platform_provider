"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const clean_webpack_plugin_1 = __importDefault(require("clean-webpack-plugin"));
const webpack_bootstrap_1 = require("./webpack.bootstrap");
const webpack_compile_1 = __importDefault(require("./webpack.compile"));
const webpack_constant_1 = __importDefault(require("./webpack.constant"));
var Common;
(function (Common) {
    const clean = new clean_webpack_plugin_1.default([webpack_constant_1.default.target.source], {
        root: webpack_constant_1.default.target.dist,
        exclude: ['report.html', 'index.html']
    });
    const plugins = [
        clean, webpack_bootstrap_1.Bootstrap.template, webpack_bootstrap_1.Bootstrap.favicon
    ];
    Common.container = {
        context: webpack_constant_1.default.target.dist,
        entry: webpack_constant_1.default.bundle.packages,
        module: { rules: webpack_compile_1.default.rules },
        resolve: {
            plugins: [],
            extensions: webpack_compile_1.default.extensions
        },
        plugins,
        output: webpack_constant_1.default.output.container,
        node: { fs: 'empty' }
    };
})(Common || (Common = {}));
exports.default = Common;
//# sourceMappingURL=webpack.common.js.map