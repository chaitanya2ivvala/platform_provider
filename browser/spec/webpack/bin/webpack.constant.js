"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
var Target;
(function (Target) {
    var _a;
    Target.project = path_1.default.resolve(__dirname, '../../../');
    Target.browser = path_1.default.resolve(Target.project, 'browser');
    Target.webpack = path_1.default.resolve(Target.browser, 'webpack');
    Target.loaders = path_1.default.resolve(Target.webpack, 'loaders', 'config');
    Target.tsconfig = path_1.default.resolve(Target.project, 'tsconfig.json');
    Target.source = path_1.default.resolve(Target.browser, 'src');
    Target.dist = path_1.default.resolve(Target.source, 'bin');
    _a = [
        path_1.default.resolve(Target.source, 'Document'), path_1.default.resolve(Target.source, 'Workers'), path_1.default.resolve(Target.browser, 'resources')
    ], Target.document = _a[0], Target.workers = _a[1], Target.resources = _a[2];
})(Target = exports.Target || (exports.Target = {}));
var Structure;
(function (Structure) {
    Structure.index = {
        path: path_1.default.resolve(Target.dist),
        filename: 'index.html'
    };
    Structure.modules = {
        name: 'node_modules',
        path: path_1.default.join(Target.project, 'node_modules')
    };
    Structure.sources = [
        {
            name: 'root',
            get: path_1.default.resolve(Structure.index.path, Structure.index.filename)
        }
    ];
})(Structure = exports.Structure || (exports.Structure = {}));
var Output;
(function (Output) {
    Output.container = {
        filename: '[name].bundle.js',
        path: Target.dist,
        publicPath: '/'
    };
})(Output = exports.Output || (exports.Output = {}));
var Application;
(function (Application) {
    Application.title = 'Vultus AB - Platform Specification';
    Application.entry = path_1.default.resolve(Structure.index.path, Structure.index.filename);
})(Application = exports.Application || (exports.Application = {}));
var Entry;
(function (Entry) {
    Entry.root = { app: path_1.default.resolve(Target.source, 'index.ts') };
})(Entry = exports.Entry || (exports.Entry = {}));
var Bundle;
(function (Bundle) {
    Bundle.packages = {
        document: [
            path_1.default.resolve(Structure.modules.path, 'swagger-ui-themes', 'themes', '3.x', 'theme-outline.css'),
            path_1.default.resolve(Target.source, 'theme.css')
        ],
        application: Entry.root.app
    };
})(Bundle = exports.Bundle || (exports.Bundle = {}));
exports.default = {
    structure: Structure,
    application: Application,
    target: Target,
    output: Output,
    bundle: Bundle,
    entry: Entry
};
//# sourceMappingURL=webpack.constant.js.map