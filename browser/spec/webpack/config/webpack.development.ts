/* eslint-disable import/no-extraneous-dependencies */

import merge from 'webpack-merge'
import webpack from 'webpack'

/* eslint-enable import/no-extraneous-dependencies */

import Common from './webpack.common'
import Constant from './webpack.constant'

const { HotModuleReplacementPlugin } = webpack
// const { ContextReplacementPlugin } = webpack

export default merge( Common.container, {
  mode     : 'development',
  plugins  : [ new HotModuleReplacementPlugin() ],
  devServer: {
    contentBase       : Constant.target.dist,
    port              : 8000,
    historyApiFallback: true,
    inline            : true,
    open              : false
  }
} )
