import _ from 'path'

/* eslint-disable typescript/no-namespace, no-undef */

export namespace Target {
  export const project = _.resolve( __dirname, '../../../' )
  export const browser = _.resolve( project, 'browser' )
  export const webpack = _.resolve( browser, 'webpack' )
  export const loaders = _.resolve( webpack, 'loaders', 'config' )
  export const tsconfig = _.resolve( project, 'tsconfig.json' )
  export const source = _.resolve( browser, 'src' )
  export const dist = _.resolve( source, 'bin' )
  export const [
    document, workers, resources
  ] = [
    _.resolve( source, 'Document' ), _.resolve( source, 'Workers' ), _.resolve( browser, 'resources' )
  ]
}

export namespace Structure {
  export const index = {
    path    : _.resolve( Target.dist ),
    filename: 'index.html'
  }
  export const modules = {
    name: 'node_modules',
    path: _.join( Target.project, 'node_modules' )
  }
  export const sources = [
    {
      name: 'root',
      get : _.resolve( index.path, index.filename )
    }
  ]
}

export namespace Output {
  export const container = {
    filename  : '[name].bundle.js',
    path      : Target.dist,
    publicPath: '/'
  }
}

export namespace Application {
  export const title = 'Vultus AB - Platform Specification'
  export const entry = _.resolve( Structure.index.path, Structure.index.filename )
}
export namespace Entry {
  export const root = { app: _.resolve( Target.source, 'index.ts' ) }
}

export namespace Bundle {
  export const packages = {
    // polyfills  : [ _.resolve( Target.resources, 'packages', 'polyfills' ) ],
    document: [
      _.resolve(
        Structure.modules.path,
        'swagger-ui-themes',
        'themes',
        '3.x',
        'theme-outline.css'
      ),
      _.resolve( Target.source, 'theme.css' )
    ],
    application: Entry.root.app
  }
}

/* eslint-enable typescript/no-namespace */

export default {
  structure  : Structure,
  application: Application,
  target     : Target,
  output     : Output,
  bundle     : Bundle,
  entry      : Entry
}

/* eslint-enable no-undef  */
