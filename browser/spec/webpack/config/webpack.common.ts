/* eslint-disable import/no-extraneous-dependencies */

import CleanWebpackPlugin from 'clean-webpack-plugin'

import {
  Configuration
} from 'webpack'

/* eslint-enable import/no-extraneous-dependencies */

import {
  Bootstrap
} from './webpack.bootstrap'
import Compile from './webpack.compile'
import Constant from './webpack.constant'

/* eslint-disable typescript/no-namespace, no-undef */

namespace Common {
  const clean = new CleanWebpackPlugin( [ Constant.target.source ], {
    root   : Constant.target.dist,
    exclude: [ 'report.html', 'index.html' ]
  } )

  const plugins = [
    clean, Bootstrap.template, Bootstrap.favicon
  ]

  export const container: Configuration = {
    context: Constant.target.dist,
    entry  : Constant.bundle.packages,
    module : { rules: Compile.rules },
    resolve: {
      plugins   : [],
      extensions: Compile.extensions
    },
    plugins,
    output: Constant.output.container,
    node  : { fs: 'empty' }
  }
}

/* eslint-enable typescript/no-namespace */
export default Common
/* eslint-enable no-undef */
