/* eslint-disable import/no-extraneous-dependencies */

import FaviconsWebpackPlugin from 'favicons-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import _ from 'path'

/* eslint-enable import/no-extraneous-dependencies */

import Constant from './webpack.constant'

/* eslint-disable typescript/no-namespace, no-undef */

export namespace Bootstrap {
  export const template = new HtmlWebpackPlugin( {
    title   : Constant.application.title,
    filename: Constant.application.entry
  } )

  export const favicon = new FaviconsWebpackPlugin( {
    logo: _.resolve(
      Constant.target.resources,
      'images',
      'favicon',
      'vultus_ab_icon.png'
    ),
    prefix: 'favicon/',
    icons : {
      android     : false,
      appleIcon   : false,
      appleStartup: false,
      coast       : false,
      favicons    : true,
      firefox     : true,
      opengraph   : false,
      twitter     : false,
      yandex      : false,
      windows     : false
    }
  } )
}

/* eslint-enable typescript/no-namespace */

export default Bootstrap

/* eslint-enable no-undef */
