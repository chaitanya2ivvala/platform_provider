/* eslint-disable import/no-extraneous-dependencies */
import CompressionPlugin from 'compression-webpack-plugin'
import UglifyJSPlugin from 'uglifyjs-webpack-plugin'

import merge from 'webpack-merge'
import webpack from 'webpack'

/* eslint-enable import/no-extraneous-dependencies */

import Common from './webpack.common'

const { DefinePlugin } = webpack

export default merge( Common.container, {
  mode   : 'production',
  stats  : { warnings: false },
  plugins: [
    new UglifyJSPlugin( { sourceMap: false } ),
    new DefinePlugin( { 'process.env.NODE_ENV': JSON.stringify( 'production' ) } ),
    new CompressionPlugin( {
      asset    : '[path].gz[query]',
      algorithm: 'gzip',
      test     : /\.js$|\.ts$|\.css$|\.html$/,
      threshold: 10240,
      minRatio : 0.8
    } )
  ]
} )
