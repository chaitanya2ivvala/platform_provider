/* eslint-disable import/no-extraneous-dependencies */

import {
  RuleSetRule
} from 'webpack'

/* eslint-enable import/no-extraneous-dependencies */

import Constant from './webpack.constant'

/* eslint-disable typescript/no-namespace, no-undef */

namespace Compile {
  export const rules: Array<RuleSetRule> = [
    {
      // Keep source-map
      enforce: 'pre',
      test   : /\.js$/,
      loaders: [ 'source-map-loader' ],
      exclude: Constant.structure.modules.path
    },

    {
      // Compile ts files
      enforce: 'post',
      test   : /\.tsx?$/,
      // exclude: Constant.structure.modules.path,
      loader : 'awesome-typescript-loader',
      options: { configFileName: Constant.target.tsconfig }
    },
    {
      // Compile html files and include them
      enforce: 'post',
      test   : /\.html$/,
      exclude: Constant.structure.modules.path,
      loader : 'html-loader'
    },
    {
      // Bundle all required css by node modules and resources
      test: /\.css$/,
      use : [ 'style-loader', 'css-loader' ]
    },
    {
      // Convert Yaml import to JSON
      test   : /\.yaml$/,
      include: [ Constant.target.project ],
      use    : [ 'file-loader?name=[name].json', 'yaml-loader' ]
    },

    {
      // Bundle all required fonts by node modules
      test   : /\.(eot|svg|ttf|woff|woff2)$/,
      include: [ Constant.structure.modules.path ],
      loader : 'file-loader?name=[name].[ext]'
    },
    {
      // Bundle all required images by node modules
      test   : /\.jpe?g$|\.ico$|\.gif$|\.png$|\.svg$|\.wav$|\.mp3$|\.mp4$/,
      include: [ Constant.structure.modules.path ],
      loader : 'file-loader?name=[name].[ext]' // Retain original file name
    },
    {
      // Compile images, supports references in html files.
      // Files > 128KB is included in bundle, < 128KB is loaded on reguest
      enforce: 'post',
      test   : /\.(jpe?g|png|gif|svg)$/i,
      exclude: Constant.structure.modules.path,
      use    : [
        {
          loader : 'url-loader',
          options: {
            limit   : 1024 * 128,
            mimetype: 'image/png'
          }
        }
      ]
    }
  ]
  export const extensions = [
    '.ts',
    '.js',
    '.html',
    '.json'
  ]
}

/* eslint-enable typescript/no-namespace */

export default Compile

/* eslint-enable no-undef */
