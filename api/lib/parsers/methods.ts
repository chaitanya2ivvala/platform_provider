//
// Assets
//

import { sᴛᴀᴛɪᴄ, ʟᴏɢɢᴇʀ, ᴜɴɪᴛʏ } from '..'

//
//
//

const { ᴘᴀʀᴛɪᴀʟ } = ᴜɴɪᴛʏ.ɢʟᴏʙᴀʟ

// ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

// info( 'Produce syntax for Express response function' )
// ... [ target, 'function', ...'arguments' ]

// ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

const ᴇxᴘʀᴇssSʏɴᴛᴀx = ᴘᴀʀᴛɪᴀʟ( ( unity, { status, body, headers } ) => {

  const { merge } = unity

  const content = merge( ...body )

  const stack = []

  stack.push( [ 'res', 'set', headers ] )
  stack.push( [ 'res', 'status', status ] )
  stack.push( [ 'res', 'send', content ] )

  return stack

}, ᴜɴɪᴛʏ )

//
// Set response for invalid methods on active paths
//

const ɪɴɪᴛɪᴀʟɪᴢᴇMᴇᴛʜᴏᴅs = ᴘᴀʀᴛɪᴀʟ( ( unity, ruleset, path ) => {

  const { partial, merge, each } = unity
  const { responses } = ruleset[ path ]

  const router = []

  each( responses, ( val, key ) => {

    router[ key ] = ᴇxᴘʀᴇssSʏɴᴛᴀx( val )

  } )
  const product = {
    titles : [[ 'method', [ 'bind', 'function', 'content' ]]],
    records: router
  }

  return product

}, ᴜɴɪᴛʏ )

// ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

// info( 'Bind data to function, Express Syntax' )

// ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

const ᴇxᴘʀᴇssAᴅᴀᴘᴛᴇʀ = ᴘᴀʀᴛɪᴀʟ( ( unity, path, method, record ) => {

  const helper = ( req, res ) => {

    const { each } = unity
    const binds = {
      res,
      req
    }
    const resources = record
    each( resources, ( resource ) => {

      const [ target, operation, data ] = resource
      binds[ target ][ operation ]( data )

    } )

  }

  const resource = [ method, [ path, helper ]]

  return resource

}, ᴜɴɪᴛʏ )

const ᴍᴇᴛʜᴏᴅsCᴏᴍᴘɪʟᴇʀ = ᴘᴀʀᴛɪᴀʟ( ( unity, [ resources ]: any, path ) => {

  const { map, toPairs } = unity

  // const { info } = ʟᴏɢɢᴇʀ( __filename, { name: 'ᴍᴇᴛʜᴏᴅsCᴏᴍᴘɪʟᴇʀ' } )

  const { records } = resources

  // info( 'Response: ✓ [ onError ]: Invalid methods | ', path )

  return map( toPairs( records ), ( [ method, record ] ) => ᴇxᴘʀᴇssAᴅᴀᴘᴛᴇʀ( path, method, record ) )

}, ᴜɴɪᴛʏ )

const sᴀᴍᴘʟᴇ = () => {}
const ᴅᴇʙᴜɢ = async() => {

  return true

}

//
//
//

export default {
  ᴍᴇᴛʜᴏᴅsCᴏᴍᴘɪʟᴇʀ,
  ɪɴɪᴛɪᴀʟɪᴢᴇMᴇᴛʜᴏᴅs,
  sᴀᴍᴘʟᴇ,
  ᴅᴇʙᴜɢ
}
