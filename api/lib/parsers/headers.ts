//
// Declarations
//

//
// Assets
//

import { sᴛᴀᴛɪᴄ, ʟᴏɢɢᴇʀ, ᴜɴɪᴛʏ } from '..'

import { ᴇʀʀᴏʀs } from './@locals'

import { ᴀssᴜʀᴇs } from '../chain'

//
//
//

const { ᴘᴀʀᴛɪᴀʟ } = ᴜɴɪᴛʏ.ɢʟᴏʙᴀʟ

// ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

// info( 'Produce syntax for Express response function' )
// ... [ target, 'function', ...'arguments' ]

// ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

const ᴇxᴘʀᴇssSʏɴᴛᴀx = ᴘᴀʀᴛɪᴀʟ( ( unity, body ) => {

  const { values } = unity
  const { status } = body

  const operations = {
    status : [ status, [ 'res', 'status' ]],
    respond: [ body, [ 'res', 'send' ]]
  }

  return values( operations )

}, ᴜɴɪᴛʏ )

//
// ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢
//
// info( 'Produce header data structure' )
//
// ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢
//

const ɪɴɪᴛɪᴀʟɪᴢᴇHᴇᴀᴅᴇʀs = ᴘᴀʀᴛɪᴀʟ( ( unity, ruleset, path ) => {

  const {
    partial, at, keys, pick, merge, map, flatMap, zip
  } = unity

  const { headers } = ruleset[ path ]

  const { ᴘᴀʀsᴇʀ } = ᴇʀʀᴏʀs

  const labs = keys( headers ),
        vals = map( headers, ( val, key ) => merge(
          {
            title: key,
            type : 'string'
          },
          val
        ) )

  const selects = [ 'title', 'enum', 'required' ]
  const extract = partial( ( select, data ) => pick( data, select ), selects )

  const logic = partial( ( select, data ) => at( data, select ), [ 'func' ] ),
        rules = flatMap( vals, logic )

  const errornums = map( labs, ( val, nr ) => 406 + ( nr + 1 ) * 10 )
  const requireds = map( map( vals, extract ), 'required' )

  const genError = ᴘᴀʀsᴇʀ( '406', headers )

  const genValid = ᴀssᴜʀᴇs.ʜᴇᴀᴅᴇʀs( zip( errornums, requireds, rules ) )

  const attach = { schemas: { headers: map( vals, extract ) } }
  const validate = { headers: zip( labs, genValid ) }
  const error = {
    generate: genError,
    attach
  }

  return {
    ...validate,
    error
  }

}, ᴜɴɪᴛʏ )

// ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

// info( 'Bind data to function, Express Syntax' )

// ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

const ᴠᴀʟɪᴅᴀᴛᴇPᴀʏʟᴏᴀᴅ = ᴘᴀʀᴛɪᴀʟ( ( unity, req, record ) => {

  const {
    map, isEqual, at, set, flatMap, isEmpty, compact, unzip
  } = unity

  const [ validators, attachments ] = unzip( record )

  const errors = compact(
    flatMap( validators, ( test ) => {

      const [ error ] = test( req )

      return error

    } )
  )

  const isValid = isEmpty( errors )

  if ( isValid )
    return []

  const isError = ( f, r ) => isEqual( f, 'send' ) && isEqual( at( r, 'details.valid' ), [ false ] )

  return map( attachments, ( [ data, [ type, func ]] ) => {

    if ( isError( func, data ) )
      set( data, 'details.errors', [ ...data.details.errors, ...errors ] )

    return [ data, [ type, func ]]

  } )

}, ᴜɴɪᴛʏ )

const ᴇxᴘʀᴇssAᴅᴀᴘᴛᴇʀ = ᴘᴀʀᴛɪᴀʟ( ( unity, path, record ) => {

  const { unzip, hover } = unity

  const { headers, error } = record
  const [ labels, validators ] = unzip( headers )

  const setPayload = ( { generate, attach }, errnos ) => {

    const { body, status } = hover( generate( errnos, attach ) ).label( 'body' )

    return ( res ) => res.status( status ).send( body )

  }

  const helper = ( req, res, next ) => {

    const { map, without, isEmpty, not, filter } = unity

    const results = map( validators, ( f ) => f( req ) )

    const valid = isEmpty( without( map( results, 'valid' ), true ) )

    const errnos = map( filter( results, [ 'allow', false ] ), ( err ) => {

      const [ errno ] = err

      return errno

    } )

    const initialize = setPayload( error, errnos )

    if ( not( valid ) )
      initialize( res )

    //
    // Reached when payload is Valid
    //

    next()

  }

  const resource = [ 'use', [ path, helper ]]

  return resource

}, ᴜɴɪᴛʏ )

//
// Bind each path
//

const ʜᴇᴀᴅᴇʀsCᴏᴍᴘɪʟᴇʀ = ᴘᴀʀᴛɪᴀʟ( ( unity, [ resources ]: any, path ) => {


  // const { info } = ʟᴏɢɢᴇʀ( __filename, { name: 'ʜᴇᴀᴅᴇʀsCᴏᴍᴘɪʟᴇʀ' } )

  // info( 'Response: ✓ [ onError ]: Invalid headers | ', path )

  //
  // Placed on path, n.b. One -> One relation
  //

  const adapters = [ ᴇxᴘʀᴇssAᴅᴀᴘᴛᴇʀ( path, resources ) ]

  /*
  const adapters = map( records, ( record ) => {

    return ᴇxᴘʀᴇssAᴅᴀᴘᴛᴇʀ( path, record )

  } ) */

  return adapters

}, ᴜɴɪᴛʏ )

const sᴀᴍᴘʟᴇ = () => {}
const ᴅᴇʙᴜɢ = async() => {

  return true

}

//
//
//

export default {
  ʜᴇᴀᴅᴇʀsCᴏᴍᴘɪʟᴇʀ,
  ɪɴɪᴛɪᴀʟɪᴢᴇHᴇᴀᴅᴇʀs,
  sᴀᴍᴘʟᴇ,
  ᴅᴇʙᴜɢ
}
