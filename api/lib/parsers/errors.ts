//
// Assets
//

import { sᴛᴀᴛɪᴄ, ʟᴏɢɢᴇʀ, ᴜɴɪᴛʏ } from '..'

//
//
//

const sᴇᴄᴜʀᴇIɴᴘᴜᴛ = ( errno ) => {

  const { castArray, compact, isEmpty } = ᴜɴɪᴛʏ

  if ( isEmpty( compact( castArray( errno ) ) ) )
    return []

  return castArray( errno )

}

const ᴄᴏᴍᴘɪʟᴇʀ = (
  error,
  templates,
  source,
  errnos,
  attachment = {},
  custom = []
) => {

  const { template, map, set, size, merge, join, isUndefined } = ᴜɴɪᴛʏ

  const errors = map( errnos, ( errno ) => {

    const { message, tag } = templates[ errno ]

    if ( isUndefined( source ) )
      return {
        errno,
        message
      }

    const values = join( source[ tag ].enum, ', ' )

    return {
      errno,
      message: template( message )( {
        tag,
        values
      } )
    }

  } )
  const all = [ ...error.details.errors, ...errors, ...custom ]
  set( error, 'details.errors', all )
  set( error, 'details.errorCount', size( all ) )

  return merge( error, attachment )

}

const ᴘᴀʀsᴇʀ = ( status, source = undefined ) => {

  const { at, partial } = ᴜɴɪᴛʏ

  const [ error ] = at( sᴛᴀᴛɪᴄ( 'errors' ), status )
  const [ templates ] = at( sᴛᴀᴛɪᴄ( 'error-templates' ), status )

  const worker = partial( ᴄᴏᴍᴘɪʟᴇʀ, error, templates, source )

  return worker

  // const tt = worker([[ 416, { header: 'Accepts', values: 'App/json' } ]])


}

export default { ᴘᴀʀsᴇʀ }
