//
// Dependencies
//

import ᴀᴊᴠ from 'ajv'

//
// Assets
//

import { sᴛᴀᴛɪᴄ, ʟᴏɢɢᴇʀ, ᴜɴɪᴛʏ } from '..'

import { ᴇʀʀᴏʀs } from './@locals'

//
//
//

const { ᴘᴀʀᴛɪᴀʟ, ᴏɴᴄᴇ } = ᴜɴɪᴛʏ.ɢʟᴏʙᴀʟ

//
// Validator for express response
// Stack: [ target, 'function', ...'arguments' ]
//

const ᴠᴀʟɪᴅᴀᴛᴏʀ = ᴏɴᴄᴇ( () => {

  const { isObject, compact, isEmpty, map, indexOf, pick, isUndefined } = ᴜɴɪᴛʏ

  const ajv = new ᴀᴊᴠ()

  const helper = ( req, res, hook ): any => {

    const check = map( [ req, res ], isObject )

    if ( isEmpty( compact( check ) ) )
      return [ false ] // 'AJV::addSchema | No schema provided.'

    const index = indexOf( check, true )

    const type = [ 'req', 'res' ][ index ],
          name = `${ type }-${ hook }`

    return [ name ]

  }

  return {
    addSchema ( { request, response }, hook ) {

      const name = helper( request, response, hook )

      if ( isEmpty( compact( name ) ) )
        return name // false

      const [ schema ] = compact( [ request, response ] )

      return ajv.addSchema( schema, ...name )

    },
    validate ( hook, { req, res } ) {

      const name = helper( req, res, hook )

      const [ data ] = compact( [ req, res ] )

      const [ id ] = name

      if ( isUndefined( ajv.getSchema( id ) ) )
        return { allow: true }

      if ( ajv.validate( id, data.body ) )
        return { allow: true }

      const priErrors = map( ajv.errors, ( e ) => pick( e, [ 'keyword', 'message' ] ) )

      return {
        allow : false,
        errnos: [ '410' ],
        errors: priErrors
      }

    }
  }

} )

const ᴡᴏʀᴋᴇʀ = ᴘᴀʀᴛɪᴀʟ( ( unity, hooks, [ method, ruleset ] ) => {

  const { pick, map, partial } = unity

  const hook = hooks[ method ]

  const extract = ( ( data ) => {

    const labels = [ 'request', 'response', 'parameters' ]
    const grouped = pick( data, labels )

    return map( grouped, 'schema' )

  } )( ruleset )

  const [ request, response, parameters ] = extract

  const { addSchema, validate } = ᴠᴀʟɪᴅᴀᴛᴏʀ()
  addSchema( { request }, hook )
  addSchema( { response }, hook )

  return partial( validate, hook )

}, ᴜɴɪᴛʏ )

//
// Abstraction logic
//

const ᴏɴAʙsᴏʟᴜᴛᴇ = ᴘᴀʀᴛɪᴀʟ( ( unity, ruleset, path, method, valtor ) => {

  const { map, partial, at, dive, omit } = unity

  const { schemas, hooks } = dive( ruleset[ path ] ).after( method )

  const omits = partial( ( f, d ) => omit( d, f ), [ 'description' ] )

  const [ request, response ] = map(
    at(
      schemas,
      map( [ 'request', 'response' ], ( type ) => `${ method }.${ type }.schema` )
    ),
    omits
  )

  const { ᴘᴀʀsᴇʀ } = ᴇʀʀᴏʀs

  return {
    [ method ]: {
      validator: valtor,
      error    : {
        generate: ᴘᴀʀsᴇʀ( 400 ),
        attach  : { schemas: { request } }
      }
    }
  }

}, ᴜɴɪᴛʏ )

const ɪɴɪᴛɪᴀʟɪᴢᴇSᴄʜᴇᴍᴀs = ᴘᴀʀᴛɪᴀʟ( ( unity, ruleset, path ) => {

  const { keys, map, toPairs, partial, zip, merge, fromPairs } = unity

  const { schemas, hooks } = ruleset[ path ]

  const prepared = map( toPairs( schemas ), partial( ᴡᴏʀᴋᴇʀ, hooks ) ),
        compiled = zip( keys( schemas ), prepared )

  const endpoints = merge(
    ...map( fromPairs( compiled ), ( valtor, method ) => {

      return ᴏɴAʙsᴏʟᴜᴛᴇ( ruleset, path, method, valtor )

    } )
  )

  return endpoints

}, ᴜɴɪᴛʏ )

const ᴇxᴘʀᴇssAᴅᴀᴘᴛᴇʀ = ᴘᴀʀᴛɪᴀʟ(
  ( unity, path, [ method, { validator, error } ] ) => {

    const { hover } = unity

    const setPayload = ( { generate, attach }, errnos, fromValidation ) => {

      const { body, status } = hover(
        generate( errnos, attach, fromValidation )
      ).label( 'body' )

      return ( res ) => res.status( status ).send( body )

    }

    const helper = ( req, res, next ) => {

      const { info } = ʟᴏɢɢᴇʀ( __filename, { name: 'ᴇxᴘʀᴇssAᴅᴀᴘᴛᴇʀ' } )

      info( `Toggled: ${ path } ${ method } helper` )
      const { not } = unity

      const result = validator( { req } )

      const { allow, errors, errnos } = result
      const valid = allow

      const initialize = setPayload( error, errnos, errors )

      if ( not( valid ) )
        initialize( res )

      next()

    }

    const resource = [ method, [ path, helper ]]

    return resource

  },
  ᴜɴɪᴛʏ
)

const sᴄʜᴇᴍᴀsCᴏᴍᴘɪʟᴇʀ = ᴘᴀʀᴛɪᴀʟ( ( unity, [ resources ]: any, path ) => {

  const { map, toPairs } = unity

  const adapters = map( toPairs( resources ), ( [ method, data ] ) => {

    return ᴇxᴘʀᴇssAᴅᴀᴘᴛᴇʀ( path, [ method, data ] )

  } )

  return adapters

}, ᴜɴɪᴛʏ )

const sᴀᴍᴘʟᴇ = () => {}
const ᴅᴇʙᴜɢ = () => {}

//
//
//

export default {
  sᴀᴍᴘʟᴇ,
  ᴅᴇʙᴜɢ,
  sᴄʜᴇᴍᴀsCᴏᴍᴘɪʟᴇʀ,
  ɪɴɪᴛɪᴀʟɪᴢᴇSᴄʜᴇᴍᴀs
}
