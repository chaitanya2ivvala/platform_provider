//
// Assets
//

import { sᴛᴀᴛɪᴄ, ʟᴏɢɢᴇʀ, ᴜɴɪᴛʏ } from '..'

const standardize = ( status ) => {

  const { has, pick, merge } = ᴜɴɪᴛʏ
  const onError = [ 'status', 'name', 'message', 'error' ]
  const onSuccess = [ 'status' ]
  const data = pick( status, has( status, 'error' ) ? onError : onSuccess )

  const general = {
    error  : false,
    status : undefined,
    name   : undefined,
    message: undefined
  }

  return merge( general, data )

}
const { partial } = ᴜɴɪᴛʏ
export default class Status {

  public it: any = partial( this.logger, this )

  public meta: any = partial( this.worker, this )

  public records = []

  private parent

  private reports: any = {}

  public constructor ( parent, status ) {

    this.records.push( standardize( { status } ) )

    const { join } = ᴜɴɪᴛʏ
    const { failure, warning, success, note } = ʟᴏɢɢᴇʀ( __filename, { name: join( parent, '.' ) } )
    this.reports = { failure, warning, success, note }

    note( 'Observes' )

  }

  public status ( status ) {

    this.records.push( standardize( { status } ) )

  }

  /*
  { [bad_request: _id field must contain a string]
  status: 400,
  name: 'bad_request',
  message: '_id field must contain a string',
  error: true } }
  */

  // eslint-disable-next-line class-methods-use-this
  private logger ( host, ...args ): any {

    const { head, isNull } = ᴜɴɪᴛʏ

    const record = head( args )

    if ( isNull( record ) )
      return void 'Everything is fine'

    host.records.push( standardize( record ) )

    return void 'Completed'

  }

  public set log ( status ) {

    this.records.push( standardize( status ) )

  }

  // eslint-disable-next-line typescript/member-ordering, class-methods-use-this
  public worker ( host ) {

    const { filter, map, compact, last, size, isEqual, merge } = ᴜɴɪᴛʏ

    const codes = merge( sᴛᴀᴛɪᴄ( 'errors' ), sᴛᴀᴛɪᴄ( 'success' ) )

    const errors = filter( host.records, [ 'error', true ] )
    const code = last( compact( map( host.records, 'status' ) ) )

    const status = codes[ code ]

    map( errors, ( { message } ) => status.details.errors.push( { message } ) )

    const errorCount = size( status.details.errors )
    const valid = isEqual( errorCount, 0 )
    const product = merge( status, { details: { valid, errorCount } } )

    host.reports.note( `[ ${ valid ? '✓' : '⚠' } Status ] 〈${ code } 〉 ` )
    if ( valid )
      return { meta: product }

    host.reports.failure( `Fail with status ${ code }, ${ status.message }` )

    return product

  }

}
