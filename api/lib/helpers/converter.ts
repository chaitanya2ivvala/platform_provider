//
// Assets
//

import { ᴜɴɪ, ᴜɴɪᴛʏ } from '..'

//
//
//

const ᴜɴɪPaths = ᴜɴɪ( ( ᴀssᴇᴛs, [ sour, expr ] ) => {

  const { toPairs, last, get, map, leam } = ᴀssᴇᴛs.ᴜɴɪᴛʏ,
        { sʀᴄ, ᴘᴜᴛ } = ᴀssᴇᴛs

  const point = `path.${ expr }`

  return [ map, [[ map, [[ toPairs, sʀᴄ( sour ) ], last ]], [[ leam, get ], ᴘᴜᴛ( point ) ]]]

} )

const ᴜɴɪNavis = ᴜɴɪ( ( ᴀssᴇᴛs, [ sour, label ] ) => {

  const { zip, values, fromPairs, prefix, fill } = ᴀssᴇᴛs.ᴜɴɪᴛʏ
  const { canFill, filter, map, leam, join, feam, size } = ᴀssᴇᴛs.ᴜɴɪᴛʏ,
        { sʀᴄ, ᴘᴜᴛ } = ᴀssᴇᴛs

  const regEx = {
    params: /(:[a-zA-z]+)/gm,
    fragms: /([a-zA-z]+)/gm
  }
  const { params, fragms } = regEx

  const labels = [ 'reference', 'fragments', 'parameters' ]

  const ᴘᴀʀᴀᴍᴇᴛᴇʀs = ᴜɴɪ( ( _, [ exp ] ) => [ map, [ sʀᴄ( sour ), [[ leam, filter ], [[ leam, canFill ], ᴘᴜᴛ( exp ) ]]]] )( params )
  const ғʀᴀɢᴍᴇɴᴛs = ᴜɴɪ( ( _, [ exp ] ) => [ map, [ sʀᴄ( sour ), [[ leam, filter ], [[ leam, canFill ], ᴘᴜᴛ( exp ) ]]]] )( fragms )
  const ʀᴇғᴇʀᴇɴᴄᴇs = ᴜɴɪ( ( _, [ deli ] ) => [ map, [[ map, [[ values, sʀᴄ( sour ) ], [[ leam, join ], ᴘᴜᴛ( deli ) ]]], [[ leam, prefix ], ᴘᴜᴛ( deli ) ]]] )( '/' )

  return [[[ feam, zip ], [[[ leam, fill ], ᴘᴜᴛ( label ) ], [ Array, [ size, ᴘᴜᴛ( sour ) ]]]], [ map, [[ map, [ sʀᴄ( zip( ʀᴇғᴇʀᴇɴᴄᴇs, ғʀᴀɢᴍᴇɴᴛs, ᴘᴀʀᴀᴍᴇᴛᴇʀs ) ), [[ feam, zip ], ᴘᴜᴛ( labels ) ]]], fromPairs ]]]

} )

const ᴜɴɪExam = ᴜɴɪ( ( ᴀssᴇᴛs, [ sour, target ] ) => {

  const { map, toPairs, zip, leam, last, defaults } = ᴀssᴇᴛs.ᴜɴɪᴛʏ,
        { feam, size, fill, get, fromPairs, identity } = ᴀssᴇᴛs.ᴜɴɪᴛʏ,
        { sʀᴄ, ᴘᴜᴛ } = ᴀssᴇᴛs

  return [ map, [[[[ feam, zip ], [[[ leam, fill ], ᴘᴜᴛ( target ) ], [ Array, [ size, ᴘᴜᴛ( sour ) ]]]], [ map, [[ map, [[ toPairs, sʀᴄ( sour ) ], last ]], [[ leam, get ], ᴘᴜᴛ( target ) ]]]], [[ leam, defaults ], ᴘᴜᴛ( [ target, {} ] ) ]]]

} )

const ᴜɴɪMeth = ᴜɴɪ( ( ᴀssᴇᴛs, [ sour, label, from ] ) => {

  const { map, toPairs, zip, leam, last, defaults, keys } = ᴀssᴇᴛs.ᴜɴɪᴛʏ,
        { feam, size, fill, get } = ᴀssᴇᴛs.ᴜɴɪᴛʏ,
        { sʀᴄ, ᴘᴜᴛ } = ᴀssᴇᴛs

  return [ map, [[[[ feam, zip ], [[[ leam, fill ], ᴘᴜᴛ( label ) ], [ Array, [ size, ᴘᴜᴛ( sour ) ]]]], [ map, [[ map, [[ map, [[ toPairs, sʀᴄ( sour ) ], last ]], [[ leam, get ], ᴘᴜᴛ( from ) ]]], keys ]]], [[ leam, defaults ], ᴘᴜᴛ( [ label, {} ] ) ]]]

} )

const ᴜɴɪPairs = ᴜɴɪ( ( ᴀssᴇᴛs, items ) => {

  const { map, zip, fromPairs } = ᴀssᴇᴛs.ᴜɴɪᴛʏ,
        { sʀᴄ, ᴘᴜᴛ } = ᴀssᴇᴛs

  return [ map, [ sʀᴄ( zip( ...items ) ), fromPairs ]]

} )

const ᴜɴɪɢʟᴇ = ( func, items ) => {

  const { partial, once } = ᴜɴɪᴛʏ

  return once( partial( func, ...items ) )

}

const { ᴘᴀʀᴛɪᴀʟ } = ᴜɴɪᴛʏ.ɢʟᴏʙᴀʟ

const ᴀssᴇᴍʙʟᴇʀ = ᴘᴀʀᴛɪᴀʟ( ( unity ) => {}, ᴜɴɪᴛʏ )

export default {
  unice ( source ) {

    const _lists = ᴜɴɪPaths( source, 'list' )
    const _navis = ᴜɴɪNavis( _lists, 'paths' )
    const _meths = ᴜɴɪMeth( source, 'methods', 'schemas' )
    const _exams = ᴜɴɪExam( source, 'examples' )

    return ᴜɴɪɢʟᴇ( ᴜɴɪPairs, [ _navis, _meths, _exams ] )

  },
  lists ( source ) {

    return ᴜɴɪPaths( source, 'list' )

  },
  navis ( source ) {

    return ᴜɴɪPaths( source, 'list' )

  },
  exams ( source ) {

    return ᴜɴɪPaths( source, 'list' )

  },
  meths ( source ) {

    return ᴜɴɪPaths( source, 'list' )

  }
}
