//
// Dependencies
//

import ʙʟᴜᴇʙɪʀᴅ from 'bluebird'

//
// Assets
//

import { ᴜɴɪᴛʏ } from '..'

//
//
//

const genSelectors = ( checks, body, params, fix = '' ) => {

  const place = 'properties'
  const prefix = 'data'
  const {
    map,
    get,
    compact,
    isUndefined,
    join,
    keys,
    values,
    zip,
    fromPairs
  } = ᴜɴɪᴛʏ
  const selectors = compact(
    map( checks, ( check ) => {

      const path = `${ place }.${ check }`
      const absolute = compact( [ fix, place ] )
      const data = get( body, join( [ ...absolute, check ], '.' ) )

      if ( isUndefined( data ) )
        return void 'No reference data'

      const items = { [ check ]: data, ...params }
      const selector = fromPairs(
        zip( map( keys( items ), ( v ) => `${ prefix }.${ place }.${ v }` ), values( items ) )
      )

      return { [ check ]: { selector } }

    } )
  )

  return selectors

}

const reportExistence = async( base, selectors, it ) => {

  const { map, toPairs, norm, keys, each, head } = ᴜɴɪᴛʏ
  const { castArray, isEmpty, not, zip, compact } = ᴜɴɪᴛʏ

  const datas = await ʙʟᴜᴇʙɪʀᴅ.all(
    map( selectors, ( record ) => {

      const [ name, selector ] = norm( toPairs( record ) )

      return base.find( selector, it )

    } )
  )

  const names = castArray( norm( map( selectors, keys ) ) )

  const hasContent = ( v ) => not( isEmpty( v.docs ) )

  const states = map( datas, hasContent )
  const index = zip( names, states )

  each( index, ( [ name, invalid ] ) => {

    if ( not( invalid ) )
      return void 'Nothing to report'

    const error = {
      error  : true,
      status : 409,
      message: `Element '${ name }' shall be uniq, at the present time the payload conflicts with existent resources under the same scope.`
    }

    it( error )

    return void 'Interation completed.'

  } )

  return isEmpty( compact( states ) )

}

const controllers = {
  features: {
    async post ( self, base, it, body, params ) {

      const checks = [ 'label', 'feature' ]
      const selectors = genSelectors( checks, body, params )

      const valid = await reportExistence( base, selectors, it )

      return valid

    },
    points: {
      async post ( self, base, it, body, params ) {

        const checks = [ 'label', 'point' ]
        const selectors = genSelectors( checks, body, params, 'data' )

        const valid = await reportExistence( base, selectors, it )

        return valid

      }
    },
    zones: {
      async post ( self, base, it, body, params ) {

        const checks = [ 'label', 'point' ]
        const selectors = genSelectors( checks, body, params )

        const valid = await reportExistence( base, selectors, it )

        return valid

      },
      points: {
        async post ( self, base, it, body, params ) {

          const checks = [ 'label', 'point' ]
          const selectors = genSelectors( checks, body, params, 'data' )

          const valid = await reportExistence( base, selectors, it )

          return valid

        }
      }
    }
  }
}

export default controllers
