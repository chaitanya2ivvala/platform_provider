import converter from './converter'
import guarantees from './guarantees'
import status from './status'

export const ɢᴜᴀʀᴀɴᴛᴇᴇs = guarantees
export const sᴛᴀᴛᴜs = status

export const ᴄᴏɴᴠᴇʀᴛᴇʀ = converter

export default { ᴄᴏɴᴠᴇʀᴛᴇʀ, guarantees, status }
