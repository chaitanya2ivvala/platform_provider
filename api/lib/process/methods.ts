//
// Assets
//

import { sᴛᴀᴛɪᴄ, ᴜɴɪᴛʏ } from '..'

import { sᴀғᴇ } from './@tools'

//
//
//

const { ᴘᴀʀᴛɪᴀʟ } = ᴜɴɪᴛʏ.ɢʟᴏʙᴀʟ

const ʜᴇᴀᴅᴇʀs = {
  Allow ( methods ) {

    const { join } = ᴜɴɪᴛʏ

    return { Allow: join( methods, ', ' ) }

  }
}

//
// Syncronize Template data with actual Path data
//

const sʏɴᴄʀᴏɴɪᴢᴇ = ᴘᴀʀᴛɪᴀʟ( ( unity, reference, path ) => {

  const { map, keys, head, set, merge, toNumber } = unity

  const compile = map( path, ( data, method ) => {

    const code = toNumber( head( keys( data.responses ) ) )

    return set( {}, `${ method }`, { code } )

  } )

  return merge( reference, merge( ...compile ) )

}, ᴜɴɪᴛʏ )

//
// Compile Error resonse on invalid method requests
//

const ᴄᴏᴍᴘɪʟᴇʀ = ᴘᴀʀᴛɪᴀʟ(
  ( unity, headers, loader, methods, allows ) => {

    const { Allow } = headers

    const { map, has, compact, merge, set, toString, at } = unity

    const errors = loader( 'errors' )

    const list = map( methods, ( { code }, method ) => {

      const isError = has( errors, toString( code ) )

      if ( isError )
        return set( {}, method, {
          status : code,
          body   : at( errors, code ),
          headers: Allow( allows )
        } )

      return false

    } )

    return merge( ...compact( list ) )

  },
  ᴜɴɪᴛʏ,
  ʜᴇᴀᴅᴇʀs,
  sᴛᴀᴛɪᴄ
)

//
// Assemble methods and responses for selected Path
//

const ᴡᴏʀᴋᴇʀ = ᴘᴀʀᴛɪᴀʟ(
  ( unity, loader, safe, path ) => {

    const { keys } = unity

    //
    // Read methods template
    //

    const secure = safe( path )

    const reference = loader( 'methods' )

    const allows = keys( secure )

    const methods = sʏɴᴄʀᴏɴɪᴢᴇ( reference, secure ),
          responses = ᴄᴏᴍᴘɪʟᴇʀ( methods, allows )

    return {
      methods,
      responses
    }

  },
  ᴜɴɪᴛʏ,
  sᴛᴀᴛɪᴄ,
  sᴀғᴇ
)

const ᴀssᴇᴍʙʟᴇʀ = ᴘᴀʀᴛɪᴀʟ( ( paths ) => {

  const { keys, map, zip, fromPairs } = ᴜɴɪᴛʏ

  const resolved = map( paths, ᴡᴏʀᴋᴇʀ ),
        endoints = keys( paths ),
        prepared = zip( endoints, resolved )

  const result = fromPairs( prepared )

  return result

} )

export default ᴀssᴇᴍʙʟᴇʀ
