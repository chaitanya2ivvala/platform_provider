//
// Assets
//

import { ᴜɴɪᴛʏ } from '..'

import { ᴅᴇʟɪ, ᴘᴀʀsᴇʀ } from './@tools'

//
//
//

const { ᴘᴀʀᴛɪᴀʟ } = ᴜɴɪᴛʏ.ɢʟᴏʙᴀʟ

const ᴀssᴇᴍʙʟᴇʀ = ᴘᴀʀᴛɪᴀʟ(
  ( unity, parser, deli, paths ) => {


    //
    // Compile syntax for | 'swagger'| 'express' | 'list' |
    //

    const prepared = ( ( ps ) => {

      const { map, head, toPairs, compact, join, partial, zip } = unity

      const worker = partial( ( d, v ) => join( v, d ), deli )
      const swg = map( toPairs( ps ), head ),
            arr = map( swg, parser ),
            exp = map( arr, worker ),
            lst = map( arr, compact )

      return zip( swg, exp, lst )

    } )( paths )

    //
    // Group and attach label
    //

    const grouped = ( ( data ) => {

      const labels = [ 'swagger', 'express', 'list' ]

      const { map, partial, zip, fromPairs } = unity

      const worker = partial( ( lbs, grp ) => fromPairs( zip( lbs, grp ) ), labels )

      return map( data, worker )

    } )( prepared )

    //
    //    Compile groups into object structure.
    // ⚿ Swagger syntax
    //

    const compiled = ( ( data ) => {

      const { map, merge, zip, fromPairs, set } = unity

      const preSet = ( group ) => {

        return set( {}, `${ group.swagger }.path`, group )

      }

      return merge( ...map( data, preSet ) )

    } )( grouped )

    return compiled

  },
  ᴜɴɪᴛʏ,
  ᴘᴀʀsᴇʀ,
  ᴅᴇʟɪ
)

export default ᴀssᴇᴍʙʟᴇʀ
