//
// Assets
//

import { sᴀғᴇ } from './@tools'

import { ᴜɴɪᴛʏ } from '..'

//
//
//

const { ᴘᴀʀᴛɪᴀʟ } = ᴜɴɪᴛʏ.ɢʟᴏʙᴀʟ

const ᴘᴀʀᴀᴍᴇᴛᴇʀs = ᴘᴀʀᴛɪᴀʟ( ( unity, parameters, target ) => {

  const { filter, partial, map, pick, fromPairs, zip } = unity

  const rule = { in: target }
  const selector = partial( ( r, d ) => filter( d, r ), rule )

  const selection = selector( parameters )

  const targets = [ 'name', 'schema' ]
  const extractor = partial( ( r, d ) => pick( d, r ), targets )

  const content = [ map( selection, 'name' ), map( selection, extractor ) ]

  return fromPairs( zip( ...content ) )

}, ᴜɴɪᴛʏ )

const ᴘᴀʀsᴇʀ = ᴘᴀʀᴛɪᴀʟ( ( unity, request ) => {

  const { isUndefined, merge } = unity

  if ( isUndefined( request ) )
    return false

  const { toPairs, flatten } = unity

  const [ contentType, { schema, example } ] = flatten( toPairs( request.content ) )

  const normalized = merge(
    {
      contentType,
      schema
    },
    isUndefined( example ) ? {} : { example }
  )

  return normalized

}, ᴜɴɪᴛʏ )

const ʀᴇǫᴜᴇsᴛ = ( request ) => ᴘᴀʀsᴇʀ( request )

const ʀᴇsᴘᴏɴsᴇ = ᴘᴀʀᴛɪᴀʟ( ( unity, responses, type ) => {

  const {
    isEqual, filter, partial, inRange, keys, map, merge, head
  } = unity
  const code = isEqual( type, 'success' ) ? 200 : 400
  const rule = partial( ( c, v, k ) => {

    return inRange( k, c, c + 100 )

  }, code )

  const t = keys( responses )

  const statusLabels = merge(
    ...map( keys( responses ), ( v ) => ( { [ v ]: { status: v } } ) )
  )

  const prepared = merge( statusLabels, responses ),
        filtered = filter( prepared, rule ),
        contents = map( filtered, ( record ) => {

          const { status } = record

          return {
            status,
            ...ᴘᴀʀsᴇʀ( record )
          }

        } )

  return head( contents )

}, ᴜɴɪᴛʏ )

const ᴡᴏʀᴋᴇʀ = ᴘᴀʀᴛɪᴀʟ(
  ( unity, safe, path ) => {

    const { zip, map, head, last, toPairs, fromPairs } = unity

    const runner = ( p ) => {

      const { parameters, requestBody, responses } = p

      return [ ᴘᴀʀᴀᴍᴇᴛᴇʀs( parameters, 'path' ), ʀᴇǫᴜᴇsᴛ( requestBody ), ʀᴇsᴘᴏɴsᴇ( responses, 'success' ) ]

    }

    const secure = toPairs( safe( path ) )

    const keys = map( secure, head ),
          vals = map( secure, last )

    const prepared = map( vals, ( record ) => {

      const [ parameters, request, response ] = [ ...runner( record ) ]

      return {
        parameters,
        request,
        response
      }

    } )

    const compiled = fromPairs( zip( keys, prepared ) )

    return { schemas: compiled }

  },
  ᴜɴɪᴛʏ,

  sᴀғᴇ
)

const ᴀssᴇᴍʙʟᴇʀ = ᴘᴀʀᴛɪᴀʟ( ( unity, paths ) => {

  const { map, keys, zip, fromPairs } = unity

  const resolved = map( paths, ᴡᴏʀᴋᴇʀ ),
        endoints = keys( paths ),
        prepared = zip( endoints, resolved )

  const result = fromPairs( prepared )

  return result

}, ᴜɴɪᴛʏ )

export default ᴀssᴇᴍʙʟᴇʀ
