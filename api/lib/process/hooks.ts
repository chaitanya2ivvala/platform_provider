//
// Assets
//

import { ᴜɴɪᴛʏ } from '..'

import { sᴀғᴇ } from './@tools'

//
//
//

const { ᴘᴀʀᴛɪᴀʟ } = ᴜɴɪᴛʏ.ɢʟᴏʙᴀʟ

const ʜᴏᴏᴋs = ᴘᴀʀᴛɪᴀʟ( ( path ) => {

  const { operationId } = path

  return [ operationId ]

} )

const ᴡᴏʀᴋᴇʀ = ᴘᴀʀᴛɪᴀʟ(
  ( unity, safe, path ) => {

    const { zip, toPairs, map, last, head, fromPairs } = unity

    const secure = toPairs( safe( path ) )

    const keys = map( secure, head ),
          vals = map( secure, last )

    const compiled = fromPairs( zip( keys, map( vals, ʜᴏᴏᴋs ) ) )

    return { hooks: compiled }

  },
  ᴜɴɪᴛʏ,
  sᴀғᴇ
)

const ᴀssᴇᴍʙʟᴇʀ = ᴘᴀʀᴛɪᴀʟ( ( unity, paths ) => {

  const { map, keys, zip, fromPairs } = unity

  const resolved = map( paths, ᴡᴏʀᴋᴇʀ ),
        endoints = keys( paths ),
        prepared = zip( endoints, resolved )

  const result = fromPairs( prepared )

  return result

}, ᴜɴɪᴛʏ )

export default ᴀssᴇᴍʙʟᴇʀ
