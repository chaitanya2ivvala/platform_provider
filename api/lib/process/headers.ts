//
// Assets
//

import { ᴜɴɪᴛʏ } from '..'

//
//
//

const { ᴘᴀʀᴛɪᴀʟ } = ᴜɴɪᴛʏ.ɢʟᴏʙᴀʟ

//
// Assemble header data for selected Path
//

const ᴡᴏʀᴋᴇʀ = ᴘᴀʀᴛɪᴀʟ( ( unity, path ) => {

  const { zip, filter, map, isEqual, partial, fromPairs } = unity

  //
  // Structure data
  //

  const valuble = filter( path.parameters, { in: 'header' } )

  const take: ( val: string ) => {} = partial( map, valuble )
  const elseFalse = partial( isEqual, true )

  const pre: any = {
    names   : take( 'name' ),
    enums   : take( 'schema.enum' ),
    defaults: take( 'schema.default' ),
    required: map( take( 'required' ), elseFalse ),
    fallback: take( 'schema.default' )
  }

  //
  // Format data
  //

  const { names, enums, defaults, required } = pre

  const { camelMultiCase } = unity.ᴄᴜsᴛᴏᴍ

  const funcs = map( zip( names, enums ), ( [ label, value ] ) => {

    return [ camelMultiCase( label, '-' ), value ]

  } )

  const labels = partial( zip, [ 'func', 'enum', 'default', 'required' ] ),
        groups = map( zip( funcs, enums, defaults, required ), ( v ) => {

          return fromPairs( labels( v ) )

        } )

  const compiled = fromPairs( zip( names, groups ) )

  return { headers: compiled }

}, ᴜɴɪᴛʏ )

const ᴀssᴇᴍʙʟᴇʀ = ᴘᴀʀᴛɪᴀʟ( ( paths ) => {

  const { map, keys, zip, fromPairs } = ᴜɴɪᴛʏ

  // console.log( map( zip( enums, defaults, required ), labels ) )

  // Content-Encoding 415
  // 406

  const resolved = map( paths, ᴡᴏʀᴋᴇʀ ),
        endoints = keys( paths ),
        prepared = zip( endoints, resolved )

  const result = fromPairs( prepared )

  return result

} )

export default ᴀssᴇᴍʙʟᴇʀ
