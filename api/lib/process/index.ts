//
// Resolvers
//

import ᴘᴀᴛʜs from './paths'

import sᴄʜᴇᴍᴀs from './schemas'
import ʜᴇᴀᴅᴇʀs from './headers'
import ʜᴏᴏᴋs from './hooks'
import ᴇxᴀᴍᴘʟᴇs from './examples'
import ᴍᴇᴛʜᴏᴅs from './methods'

export default {
  ᴍᴇᴛʜᴏᴅs,
  ᴘᴀᴛʜs,
  ʜᴇᴀᴅᴇʀs,
  sᴄʜᴇᴍᴀs,
  ʜᴏᴏᴋs,
  ᴇxᴀᴍᴘʟᴇs
}
