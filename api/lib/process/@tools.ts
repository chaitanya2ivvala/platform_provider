//
// Assets
//

import { sᴛᴀᴛɪᴄ, ᴜɴɪᴛʏ } from '..'

//
//
//

const { ᴘᴀʀᴛɪᴀʟ } = ᴜɴɪᴛʏ.ɢʟᴏʙᴀʟ

export const ᴅᴇʟɪ = '/'

export const ᴘᴀʀsᴇʀ = ᴘᴀʀᴛɪᴀʟ(
  ( unity, regex, deli, target ) => {

    const { replace, trim, partial, split, map, join } = unity

    const worker = partial(
      ( _regex, _operator, record ) => {

        return replace( record, _regex, _operator )

      },
      regex,
      ( v ) => `:${ trim( v, '{}' ) }`
    )

    const list = map( split( target, deli ), worker )

    return list

  },
  ᴜɴɪᴛʏ,
  /({[a-zA-Z]+})/,
  ᴅᴇʟɪ
)

export const sᴀғᴇ = ᴘᴀʀᴛɪᴀʟ(
  ( unity, loader, file ) => {

    const { keys, pick } = unity

    const methods = loader( file )
    const valid = keys( methods )

    return ( data ) => {

      return pick( data, valid )

    }

  },
  ᴜɴɪᴛʏ,
  sᴛᴀᴛɪᴄ
)( 'methods' )

export const ᴄᴏɴᴠᴇʀᴛ = ᴘᴀʀᴛɪᴀʟ(
  ( unity, deli, path ) => {

    const { join } = unity

    return join( ᴘᴀʀsᴇʀ( path ), deli )

  },
  ᴜɴɪᴛʏ,
  ᴅᴇʟɪ
)
