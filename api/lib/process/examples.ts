//
// Assets
//

import { sᴄʜᴇᴍᴀs } from './@locals'

import { ᴜɴɪᴛʏ } from '..'

//
//
//

const { ᴘᴀʀᴛɪᴀʟ } = ᴜɴɪᴛʏ.ɢʟᴏʙᴀʟ

const ᴀssᴇᴍʙʟᴇʀ = ᴘᴀʀᴛɪᴀʟ( ( unity, paths ) => {

  const { map, toPairs, at, compact, isEmpty, merge } = unity

  const schemas = sᴄʜᴇᴍᴀs( paths )

  const result = merge(
    ...compact( [ ...map( toPairs( schemas ), ( [ path, val ] ) => {

      const onPath = compact(
        ...map( at( val, 'schemas' ), ( rec ) => {

          const pairs = toPairs( rec )

          return [ ...map( pairs, ( [ method, records ] ) => {

            const [ example ] = compact( at( records, 'request.example' ) )

            if ( isEmpty( example ) )
              return false

            return { [ method ]: example }

          } ) ]

        } )
      )

      if ( isEmpty( onPath ) )
        return false

      return { [ path ]: { examples: merge( ...onPath ) } }

    } ) ] )
  )

  return result

}, ᴜɴɪᴛʏ )

export default ᴀssᴇᴍʙʟᴇʀ
