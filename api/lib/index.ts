import loader from '../@static'
import unity from '../../unity'
import ᴜᴛɪʟɪᴛɪᴇs from '../../utilities'

const { logger, uni, identity } = ᴜᴛɪʟɪᴛɪᴇs

export const ᴜɴɪ = uni
export const ᴜɴɪᴛʏ = unity
export const ʟᴏɢɢᴇʀ = logger
export const sᴛᴀᴛɪᴄ = loader
export const ɪᴅᴇɴᴛɪᴛʏ = identity

export default ᴜᴛɪʟɪᴛɪᴇs
