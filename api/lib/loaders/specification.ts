//
// Dependencies
//

import sᴄʜᴇᴍᴀPᴀʀsᴇʀ from 'json-schema-ref-parser'
import ʏᴀᴍʟ from 'js-yaml'
import ғs from 'fs'
import ᴘᴀᴛʜ from 'path'

//
// Assets
//

import { ᴜɴɪᴛʏ } from '..'

//
//
//

const { ᴘᴀʀᴛɪᴀʟ } = ᴜɴɪᴛʏ.ɢʟᴏʙᴀʟ

//
// Specification
//

const ᴅɪʀ = __dirname
const ʟᴀʙᴇʟ = 'swagger'
const ғᴏʀᴍᴀᴛ = 'yaml'

const ᴀssᴀᴍʙʟᴇʀ = {
  async dereference ( parser, json ) {

    return parser.dereference( json, { dereference: { circular: 'ignore' } } )

  },

  async bundle ( json ) {

    const parser: any = sᴄʜᴇᴍᴀPᴀʀsᴇʀ

    return parser.bundle( json, { dereference: { circular: 'ignore' } } )

  },

  async bundler ( content ): Promise<JSON> {


    //
    // Dependencies
    //

    const { assambler, partial, schemaParser, yaml } = {
      assambler   : ᴀssᴀᴍʙʟᴇʀ,
      partial     : ᴘᴀʀᴛɪᴀʟ,
      schemaParser: sᴄʜᴇᴍᴀPᴀʀsᴇʀ,
      yaml        : ʏᴀᴍʟ
    }

    //
    // Assets
    //

    const parser: any = schemaParser

    let dereferencedJSON,
        bundledJSON

    //
    //
    //

    try {

      dereferencedJSON = await partial( assambler.dereference, parser )( content )

    }
    catch ( error ) {

      console.error(
        'Can not dereference the JSON obtained from the content of the Swagger specification file'
      )
      console.error( error )

      return error

    }

    try {

      bundledJSON = await assambler.bundle( dereferencedJSON )

    }
    catch ( error ) {

      console.error(
        'Can not bundle the JSON obtained from the content of the Swagger specification file'
      )
      console.error( error )

      return error

    }

    // eslint-disable-next-line consistent-return
    return JSON.parse( JSON.stringify( bundledJSON ) )

  }
}

const ᴛᴀʀɢᴇᴛ = ᴘᴀʀᴛɪᴀʟ( ( { join }, dir, label, format ) => {

  return join( dir, '../../', label, `${ label }.${ format }` )

}, ᴘᴀᴛʜ )( ᴅɪʀ, ʟᴀʙᴇʟ, ғᴏʀᴍᴀᴛ )

const sᴘᴇᴄ = ᴘᴀʀᴛɪᴀʟ(
  ( yaml, fs, { join }, dir, label, format ) => {

    const target = join( dir, '../../', label, `${ label }.${ format }` )

    try {

      // eslint-disable-next-line security/detect-non-literal-fs-filename
      return yaml.safeLoad( fs.readFileSync( target, 'utf8' ) )

    }
    catch ( error ) {

      return error

    }

  },
  ʏᴀᴍʟ,
  ғs,
  ᴘᴀᴛʜ
)( ᴅɪʀ, ʟᴀʙᴇʟ, ғᴏʀᴍᴀᴛ )

const ʀᴇsᴏʟᴠᴇᴅ = ᴘᴀʀᴛɪᴀʟ( ( assambler, spec ) => {

  return assambler.bundler( spec )

}, ᴀssᴀᴍʙʟᴇʀ )( sᴘᴇᴄ )

/*
    spec: sᴘᴇᴄ,
  path: ᴛᴀʀɢᴇᴛ,
*/

export default ʀᴇsᴏʟᴠᴇᴅ
