//
// Assets
//

import { sᴘᴇᴄɪғɪᴄᴀᴛɪᴏɴ } from './@locals'

import { ʟᴏɢɢᴇʀ, ᴜɴɪᴛʏ } from '..'

import { ᴘʀᴏᴄᴇss } from '../chain'

//
//
//

const { ᴏɴᴄᴇ } = ᴜɴɪᴛʏ.ɢʟᴏʙᴀʟ

const ruleset = async() => {

  const { success } = ʟᴏɢɢᴇʀ( __filename, ruleset )

  const resolved = await sᴘᴇᴄɪғɪᴄᴀᴛɪᴏɴ

  const { paths } = resolved as any

  const { zip, fill, size, inarmPairs } = ᴜɴɪᴛʏ

  const { ᴘᴀᴛʜs, ᴍᴇᴛʜᴏᴅs, ʜᴇᴀᴅᴇʀs, sᴄʜᴇᴍᴀs, ʜᴏᴏᴋs, ᴇxᴀᴍᴘʟᴇs } = ᴘʀᴏᴄᴇss

  const resolvers = [
    ᴘᴀᴛʜs,
    ᴍᴇᴛʜᴏᴅs,
    ʜᴇᴀᴅᴇʀs,
    sᴄʜᴇᴍᴀs,
    ʜᴏᴏᴋs,
    ᴇxᴀᴍᴘʟᴇs
  ]

  const resource = fill( new Array( size( resolvers ) ), paths ),
        rulesets = inarmPairs( zip( resolvers, resource ) )

  success( 'Synchronization: ✓ ⤑ Handing over product' )

  return rulesets

}

export const ʀᴜʟᴇsᴇᴛ = ruleset

export default ᴏɴᴄᴇ( () => {

  const { note } = ʟᴏɢɢᴇʀ( __filename, { name: 'sᴘᴇᴄɪғɪᴄᴀᴛɪᴏɴ' } )

  note( 'ᴏɴʟʏ ᴏɴᴄᴇ: Will synchronize with specification...' )

  return ruleset()

} )
