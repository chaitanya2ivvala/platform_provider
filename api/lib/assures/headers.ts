//
// Assets
//

import { ʟᴏɢɢᴇʀ, ᴜɴɪᴛʏ } from '..'

//
//
//

const { ᴘᴀʀᴛɪᴀʟ } = ᴜɴɪᴛʏ.ɢʟᴏʙᴀʟ

const ᴄʜᴇᴄᴋHᴇᴀᴅᴇʀ = ᴘᴀʀᴛɪᴀʟ( ( unity, errno, required, rule ) => {

  const { info } = ʟᴏɢɢᴇʀ( __filename, { name: 'ᴏɴHᴇᴀᴅᴇʀ' } )

  const { castArray, compact, isEmpty, join, set } = unity

  return ( req ) => {

    const [ test, valids ] = rule

    const answer = req[ test ]( valids )

    //
    // Constructs error if header are invalid
    //

    const valid = !isEmpty( compact( castArray( answer ) ) )

    const result = []
    set( result, 'allow', valid || !required )
    set( result, 'valid', valid )
    set( result, 'required', required )
    set( result, 'response', answer )

    if ( !( valid || !required ) )
      result.push( errno )

    return result

  }

}, ᴜɴɪᴛʏ )

const ᴀssᴇᴍʙʟᴇʀ = ( ruleset ) => {

  const { map } = ᴜɴɪᴛʏ

  const validators = map( ruleset, ( [ errno, required, rule ] ) => {

    return ᴄʜᴇᴄᴋHᴇᴀᴅᴇʀ( errno, required, rule )

  } )

  return validators

}

export default ᴀssᴇᴍʙʟᴇʀ
