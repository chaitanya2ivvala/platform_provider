//
// Dependencies
//

import ᴀxɪᴏs from 'axios'

//
// Assets
//

import { ʜᴇʟᴘᴇʀs, ʟᴏɢɢᴇʀ, ᴜɴɪᴛʏ } from '..'

//
//
//

const build = ( status ) => {

  const { sᴛᴀᴛᴜs } = ʜᴇʟᴘᴇʀs
  // eslint-disable-next-line new-cap
  const observe = new sᴛᴀᴛᴜs( [ 'authentication', 'engine' ], status )

  return observe

}

const messenger = ( observe, tag ) => {

  const messages = {
    token:
      'Request must contain an Authorization header which must contain an Bearer token',
    bearer : 'Payload of Authorization header must contain an Bearer prefix',
    payload:
      'Payload of Authorization header must contain an Bearer with an alfanumeric token'
  }

  observe.it( { error: true, status: 406, message: messages[ tag ] } )

  const { meta } = observe

  return meta()

}

const ʙᴇᴀʀᴇʀ = ( req, res ) => {

  const {
    isUndefined,
    split,
    difference,
    isEmpty,
    compact,
    size,
    partial,
    isEqual,
    join
  } = ᴜɴɪᴛʏ

  const label = 'Authorization',
        token = req.get( label ),
        observe = build( 100 )

  const msg = partial( messenger, observe )

  if ( isUndefined( token ) )
    res.status( 406 ).send( msg( 'token' ) )

  const header = [ 'Bearer' ],
        bearer = compact( split( token, ' ' ) )

  if ( isEmpty( bearer ) )
    res.status( 406 ).send( msg( 'bearer' ) )

  if ( isEqual( size( bearer ), 1 ) )
    res.status( 406 ).send( msg( 'payload' ) )

  const [ code ] = compact( difference( bearer, header ) )

  return [ label, join( [ ...header, code ], ' ' ) ]

}

const validate = ( { status }, res ) => {

  const { indexOf, isEqual, not } = ᴜɴɪᴛʏ

  const triggers = [ 401 ]

  const { meta } = build( status )

  if ( not( isEqual( indexOf( triggers, status ), -1 ) ) )
    res.status( status ).send( meta() )

  return void 'ɴᴏᴛʜɪɴɢ ᴛᴏ ᴘᴀss'

}

const ᴀssᴇᴍʙʟᴇʀ = ( req, res, next ) => {

  const { join, isEqual, not } = ᴜɴɪᴛʏ
  const service = join( [ process.env.AUTHORIZATION_URL, 'api', 'userinfo' ], '/' )

  const { failure, success, info } = ʟᴏɢɢᴇʀ( __filename, { name: 'authentication' } )

  info( 'Init', service )

  const secret = req.get( 'Secret' )

  const sec = process.env.APPLICATION_SECRET

  if ( isEqual( secret, sec ) )
    return void next()

  //
  // Example
  // Authorization: Bearer 0b79bab50daca910b000d4f1a2b675d604257e42
  //

  //
  // Check if Authorization header is valid
  //

  const bearer = ʙᴇᴀʀᴇʀ( req, res )

  const [ lbl, bea ] = bearer

  const headers = { [ lbl ]: bea }

  ᴀxɪᴏs( {
    method        : 'get',
    url           : service,
    headers,
    validateStatus: ( status ) => true
  } )
    .then( ( response ) => {

      validate( response, res )

      //
      // Valid if reached
      //

      next()

      return void 'ɴᴏᴛʜɪɴɢ ᴛᴏ ᴘᴀss'

    } )
    .catch( ( error ) => {

      console.log( 'Auth', error )

      throw new Error( 'Auth error' )

    } )

}

export default ᴀssᴇᴍʙʟᴇʀ
