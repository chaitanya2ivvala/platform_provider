//
// Global singelton
//

import { ɪᴅᴇɴᴛɪᴛʏ, ᴜɪᴅ, ᴜɴɪᴛʏ } from '..'

//
//
//

const features = {
  relationsship () {

    const { attach } = ɪᴅᴇɴᴛɪᴛʏ

    return attach( 'feature', { relationships: [], attributes: {} }, ᴜɪᴅ( 12 ) )

  },
  enable () {

    const { set } = ᴜɴɪᴛʏ

    return set( {}, 'data.properties.enabled', true )

  },
  disable () {

    const { set } = ᴜɴɪᴛʏ

    return set( {}, 'properties.enabled', false )

  }
}

const filters = {
  features ( relationships ) {

    const { filter, norm, omit, merge, isEqual } = ᴜɴɪᴛʏ

    return norm( filter( relationships, [ 'type', 'feature' ] ) )

  },
  zones ( relationships ) {

    const { filter, norm, omit, merge, isEqual } = ᴜɴɪᴛʏ

    return norm( filter( relationships, [ 'type', 'zone' ] ) )

  }
}

const points = {
  relationsship ( identity ) {

    const { attach } = ɪᴅᴇɴᴛɪᴛʏ

    const parent = filters.features( identity.relationships )
    const zones = filters.zones( identity.relationships )

    const { set, compact, isEmpty, not } = ᴜɴɪᴛʏ
    const id = ᴜɪᴅ( 12 )

    const relationship = attach(
      'point',
      {
        relationships: compact( [ zones, parent ] ),
        attributes   : { type: 'time-range' }
      },
      id
    )
    let placement: any = { feature: parent.id, point: id }
    if ( not( isEmpty( zones ) ) )
      placement = { ...placement, zone: zones.id }

    const populate = set( {}, 'data.properties', placement )

    return { ...populate, ...relationship }

  },
  enable () {

    const { set } = ᴜɴɪᴛʏ

    return set( {}, 'data.properties.enabled', true )

  },
  disable () {

    const { set } = ᴜɴɪᴛʏ

    return set( {}, 'properties.enabled', false )

  }
}

const zones = {
  relationsship ( identity ) {

    const { attach } = ɪᴅᴇɴᴛɪᴛʏ

    const parent = filters.features( identity.relationships )

    const id = ᴜɪᴅ( 12 )

    const relationship = attach(
      'zone',
      { relationships: [ parent ], attributes: {} },
      id
    )
    const { set } = ᴜɴɪᴛʏ

    const placement = { feature: parent.id, zone: id }
    const populate = set( {}, 'data.properties', placement )

    return { ...populate, ...relationship }

  },
  enable () {

    const { set } = ᴜɴɪᴛʏ

    return set( {}, 'data.properties.enabled', true )

  },
  disable () {

    const { set } = ᴜɴɪᴛʏ

    return set( {}, 'properties.enabled', false )

  }
}

const payload = {
  articles: {
    features: {
      post ( { body }, identity ) {

        const { merge, head, set, get } = ᴜɴɪᴛʏ
        const { relationsship, enable } = features
        const group = { ...identity, data: body } // Dev id

        const product = merge( group, relationsship() )

        const { id } = head( product.relationships )

        const properties = {
          ...get( product, 'data.properties' ),
          feature: id,
          enabled: true
        }

        set( product, 'data.properties', properties )

        return product

      },
      subscription: {
        get ( { entry, check }, identity ) {

          const { get, isUndefined, omit, merge, isEqual } = ᴜɴɪᴛʏ
          const { subscription, relationships } = check

          const relationship = {
            ...filters.features( relationships ),
            attributes: { type: 'subscription' }
          }
          const attributes = subscription

          const product = {
            ...entry,
            attributes,
            relationships: [ relationship ]
          }

          return product

        },

        put ( { entry, prepare, relationships }, identity ) {

          const { get, isUndefined, omit, merge, isEqual } = ᴜɴɪᴛʏ

          const relationship = merge( filters.features( relationships ), { attributes: { type: 'subscription' } } )

          const attributes = prepare

          return { ...entry, attributes, relationships: [ relationship ] }

        },

        patch ( { entry, prepare, relationships }, identity ) {

          const { get, isUndefined, omit, merge, isEqual } = ᴜɴɪᴛʏ

          const relationship = merge( filters.features( relationships ), { attributes: { type: 'subscription' } } )

          const attributes = prepare

          return { ...entry, attributes, relationships: [ relationship ] }

        }
      },
      points: {
        post ( { body }, identity ) {

          const { relationships } = points.relationsship( identity )
          const { relationsship, enable } = points

          const product = { ...enable(), ...relationsship( identity ) }

          const { merge, prettiest } = ᴜɴɪᴛʏ

          const prepared = prettiest( merge( body, product ), 'data.properties', [
            'label',
            'point',
            'feature',
            'enabled',
            'icon'
          ] )

          return { ...prepared, relationships }

        }
      },
      zones: {
        post ( { body }, identity ) {

          const { relationships } = features.relationsship()

          const { relationsship, enable } = zones

          const { merge, prettiest } = ᴜɴɪᴛʏ

          const relations = merge(
            { ...relationsship( { relationships } ) },
            { ...enable() }
          )

          const prepared = prettiest(
            merge( { data: body }, relations ),
            'data.properties',
            [ 'label', 'zone', 'feature', 'enabled' ]
          )

          return prepared

        },
        points: {
          post ( { body }, identity ) {

            const { relationships } = points.relationsship( identity )
            const { relationsship, enable } = points

            const product = { ...enable(), ...relationsship( identity ) }

            const { merge, prettiest } = ᴜɴɪᴛʏ

            const prepared = prettiest(
              merge( body, product ),
              'data.properties',
              [
                'label',
                'zone',
                'point',
                'feature',
                'enabled',
                'icon'
              ]
            )

            return { ...prepared, relationships }

          }
        }
      }
    }
  }
}

export default payload
