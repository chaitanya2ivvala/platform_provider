//
// Assets
//

import { ᴇɴɢɪɴᴇ, ᴛʀᴀɴsғᴏʀᴍᴇʀs } from './@locals'
import { ᴜɴɪᴛʏ } from '..'

const prepare = ( source, child ) => {

  const {
    compact,
    split,
    filter,
    map,
    leam,
    canFill,
    reverse,
    identity
  } = ᴜɴɪᴛʏ

  const filler = ( _source, _child, reg ) => {

    const mapped = map( _source, leam( canFill )( reg.p ) ),
          prepar = filter( _source, leam( canFill )( reg.f ) )

    const re = reverse
    const absolut = map( prepar, identity )

    return [ absolut, re( map( re( mapped ), ( is ) => ( is ? _child.pop() : prepar.pop() ) ) ) ]

  }

  const regEx = {
    p: /(:[a-zA-z]+)/gm,
    f: /([a-zA-z]+)/gm
  }

  return filler( compact( split( source, '/' ) ), child, regEx )

}

const ɴᴀᴠɪɢᴀᴛᴏʀ = async( item ) => {

  const {
    get,
    isUndefined,
    identity,
    map,
    toLower,
    split,
    without,
    values
  } = ᴜɴɪᴛʏ
  const [ method, path, body, params ] = item

  const [ statis, pointer ] = prepare( path, map( params, identity ) )

  const { articles } = ᴇɴɢɪɴᴇ
  const transfomers = ᴛʀᴀɴsғᴏʀᴍᴇʀs

  const target = without( [ ...statis, toLower( method ) ], ...values( params ) )

  const origin = 'articles'

  const worker = get( articles, target )
  const transf = get( transfomers, [ origin, ...target ] )

  const final = isUndefined( transf ) ? identity : transf

  if ( isUndefined( worker ) ) {

    console.log( 'No DB handler found for path.', method, path )

    return false

  }

  const product = await worker( body, params, final, target )

  // await createExamples( product, body, target, params, statis )

  return product

}

export default ɴᴀᴠɪɢᴀᴛᴏʀ
