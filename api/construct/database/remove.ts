import { ʀᴜɴɴᴇʀ } from './engine'

import { ʟᴏɢɢᴇʀ, ᴜɴɪᴛʏ } from '..'

const target = [ 'examples', '9wwjn0liuez' ]

const ᴛᴇʀᴍɪɴᴀᴛᴇ = async( db ) => {

  const { failure, warning, success } = ʟᴏɢɢᴇʀ( __filename, ᴛᴇʀᴍɪɴᴀᴛᴇ )

  return db
    .destroy()
    .then( ( response ) => {

      return success( 'Database terminated' )

    } )
    .catch( ( error ) => {

      return failure( 'Database termination failure. Details: ', error )

    } )

}

const databases = {
  labels: [ 'features', 'points', 'zones' ],
  async connect ( self = 'all' ) {

    const { failure, warning, success, info } = ʟᴏɢɢᴇʀ( __filename, { name: 'databases.connect' } )

    const { labels } = databases
    const { last, difference, isEqual, not, isUndefined, join } = ᴜɴɪᴛʏ

    const label = last( difference( self, difference( self, labels ) ) )

    if ( not( isUndefined( label ) ) )
      info( `Selected '${ label }'` )
    else
      info( `Selected stack 〈${ join( labels, ' 〉〈' ) } 〉` )

    const all = await ʀᴜɴɴᴇʀ( labels )
    const selected = isEqual( self, 'all' ) ? all : all[ label ]

    return selected

  }
}

// const { sɪᴍᴜʟᴀᴛᴏʀ } = ʀᴇsᴏᴜʀᴄᴇs

// sɪᴍᴜʟᴀᴛᴏʀ( ᴘʀɪɴᴛᴇᴅ( ...target ) )

const { values, map, initial } = ᴜɴɪᴛʏ

const runner = async() => {

  const { connect, terminate } = { ...databases, terminate: ᴛᴇʀᴍɪɴᴀᴛᴇ }

  const dbs = await connect()

  map( values( dbs ), terminate )

}

runner()
