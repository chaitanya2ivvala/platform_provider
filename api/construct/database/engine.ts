//
// Modules
//

import PouchDB from 'pouchdb'
import pouchDebug from 'pouchdb-debug'
import pouchFind from 'pouchdb-find'

import ʙʟᴜᴇʙɪʀᴅ from 'bluebird'

import { extendMoment } from 'moment-range'
import ᴍᴏᴍᴇɴᴛ from 'moment'

//
// Assets
//

import { ʜᴇʟᴘᴇʀs, ʟᴏɢɢᴇʀ, ᴜɴɪᴛʏ } from '..'

//
//
//

const { once } = ᴜɴɪᴛʏ

const validTimeRange = ( { startAt, endAt }, observe: any ) => {

  const { not } = ᴜɴɪᴛʏ
  if ( ᴍᴏᴍᴇɴᴛ( startAt ).isAfter( endAt ) ) {

    observe.it( {
      error  : true,
      status : 400,
      message: 'Subscription range must be directed forward in time.'
    } )

    return false

  }

  return true

}

const rangeParser = ( ᴍ, ranges ) => {

  const { map } = ᴜɴɪᴛʏ

  return map( ranges, ( { timeRange } ) => ᴍ.range( [ ᴍ( timeRange.startAt, 'YYYY-MM-DD' ), ᴍ( timeRange.endAt, 'YYYY-MM-DD' ) ] ) )

}
const normalizeSubscription = ( enables, disabled = [] ) => {

  const { map, difference, partial, uniq, flatMap, zip } = ᴜɴɪᴛʏ
  const ᴍ = extendMoment( ᴍᴏᴍᴇɴᴛ as any )

  const p = partial( rangeParser, ᴍ )

  const enList = uniq(
    flatMap( p( enables ), ( r ) => [ ...r.by( 'days' ) ].map( ( m: any ) => m.format( 'YYYY-MM-DD' ) ) )
  )
  const diList = uniq(
    flatMap( p( disabled ), ( r ) => [ ...r.by( 'days' ) ].map( ( m: any ) => m.format( 'YYYY-MM-DD' ) ) )
  )

  const list = difference( enList, diList )

  const ref = map( list, ( v ) => ᴍ( v, 'YYYY-MM-DD' )
    .add( 1, 'day' )
    .format( 'YYYY-MM-DD' ) )

  const product = zip(
    uniq( difference( list, ref ) ),
    uniq(
      map( difference( ref, list ), ( v ) => ᴍ( v, 'YYYY-MM-DD' )
        .subtract( 1, 'day' )
        .format( 'YYYY-MM-DD' ) )
    )
  )

  return map( product, ( [ startAt, endAt ] ) => ( { timeRange: { startAt, endAt } } ) )

}

const createIndex = ( db, fields, name ) => {

  return db.createIndex( { index: { fields, name } } )

}
const ᴇɴɢɪɴᴇ = once( async( labels ) => {

  const { map, fromPairs, zip, join, defaultTo } = ᴜɴɪᴛʏ

  const endpoint = process.env.COUCHDB_URI
 
  const { success } = ʟᴏɢɢᴇʀ( __filename, ᴇɴɢɪɴᴇ )

  PouchDB.plugin( pouchFind )

  const instances = await ʙʟᴜᴇʙɪʀᴅ.all(
    map( labels, ( lbl ) => new PouchDB( `${ endpoint }${ lbl }` ) )
  )

  const databases = fromPairs( zip( labels, instances ) )

  //
  // Index list
  //

  const { features, points, zones } = databases
  await createIndex( features, [ 'data.properties.feature' ], 'feature' )
  await createIndex( features, [ 'data.properties.label' ], 'featureLabel' )
  await createIndex( points, [ 'data.properties.feature' ], 'feature' )
  await createIndex( points, [ 'data.properties.point' ], 'point' )
  await createIndex( points, [ 'data.properties.label' ], 'pointLabel' )
  await createIndex( zones, [ 'data.properties.zone' ], 'zone' )
  await createIndex( zones, [ 'data.properties.label' ], 'zoneLabel' )
  await createIndex( zones, [ 'data.properties.zone.point' ], 'zonepoint' )

  success( `[ ✓ Databases ] 〈${ join( labels, ' 〉〈' ) } 〉` )

  return databases

} )

export const ʀᴜɴɴᴇʀ = ᴇɴɢɪɴᴇ

const ᴛᴇsᴛɪɴɢ = once( async() => {

  const setup = {
    target : 'features',
    options: {}
  }

  const { target, options } = setup

  const { identity } = ᴜɴɪᴛʏ

  // Ensure to load

  // identity( pouchMemory )

  //
  // Included plugins
  //

  PouchDB.plugin( pouchDebug )
  PouchDB.plugin( pouchFind )

  const db = await new PouchDB( target, options )

  // PouchDB.debug.enable( '*' )

  const { failure, warning, success } = ʟᴏɢɢᴇʀ( __filename, ᴛᴇsᴛɪɴɢ )

  success( `[ ✓ Database ] 〈${ target } 〉` )

  const feature = await createIndex( db, [ 'data.properties.feature' ], 'feature' )

  return db

} )

const ᴛᴇʀᴍɪɴᴀᴛᴇ = async( db ) => {

  const { failure, warning, success } = ʟᴏɢɢᴇʀ( __filename, ᴛᴇsᴛɪɴɢ )

  return db
    .destroy()
    .then( ( response ) => {

      return success( 'Database terminated' )

    } )
    .catch( ( error ) => {

      return failure( 'Database termination failure. Details: ', error )

    } )

}

const statics = {
  article: {
    features: {
      post        : { origin: 'article' },
      get         : { origin: 'article' },
      put         : { origin: 'article' },
      patch       : { origin: 'article' },
      subscription: { put: { origin: 'feature' } }
    }
  }
}

const identifier = ( record, origin = null ) => {

  const { merge, compact, mapKeys } = ᴜɴɪᴛʏ

  const { id, rev } = record

  const entity = merge( {}, ...compact( [ { id, rev }, origin ] ) )

  const prefixed = mapKeys( entity, ( v, k ) => `_${ k }` )

  const normfix = {
    get ( obj, prop ) {

      const { indexOf, isEqual } = ᴜɴɪᴛʏ
      const keys = [ 'id', 'rev' ]

      const i = indexOf( keys, prop )

      if ( isEqual( i, -1 ) )
        return obj[ prop ]

      return obj[ `_${ prop }` ]

    }
  }

  return new Proxy( prefixed, normfix )

}

const relation = ( record ) => {

  const { pick } = ᴜɴɪᴛʏ
  const product = pick( record, [ '_id', '_rev' ] )

  return product

}

const bindRelations = ( type, { relationships } ) => {

  const { merge } = ᴜɴɪᴛʏ

  const relationship = relationships.pop()

  relationships.push( merge( relationship, { attributes: { type } } ) )

  return relationships

}

const find: any = {
  process ( { docs }, serv ) {

    const { failure, warning, success } = ʟᴏɢɢᴇʀ( __filename, { name: 'find.process' } )

    const { size, isEqual, not, last } = ᴜɴɪᴛʏ

    const elements = size( docs )

    if ( isEqual( elements, 0 ) ) {

      serv.it( [ {
        error  : true,
        status : 404,
        message:
            'Unable to fetch resource consistent with provided parameters.'
      } ] )

      return []

    }

    if ( not( isEqual( elements, 1 ) ) )
      warning(
        `Expected one (1) doc, found ${ elements }. Last doc will be returned.`
      )

    const normfix = {
      get ( obj, prop ) {

        const { indexOf } = ᴜɴɪᴛʏ
        const keys = [ 'id', 'rev' ]

        const i = indexOf( keys, prop )

        if ( isEqual( i, -1 ) )
          return obj[ prop ]

        return obj[ `_${ prop }` ]

      }
    }

    return new Proxy( last( docs ), normfix )

  },
  feature ( params ) {

    const { fromPairs, zip, map, keys, values } = ᴜɴɪᴛʏ

    const selector = fromPairs(
      zip( map( keys( params ), ( v ) => `data.properties.${ v }` ), values( params ) )
    )

    return { selector }

  },
  point ( params ) {

    const { fromPairs, zip, map, keys, values, difference, merge } = ᴜɴɪᴛʏ

    const labels = keys( params )

    const selector = fromPairs( [ ...zip( map( keys( params ), ( v ) => `data.properties.${ v }` ), values( params ) ) ] )

    return { selector }

  },
  zone ( params ) {

    const { fromPairs, zip, map, keys, values } = ᴜɴɪᴛʏ

    const selector = fromPairs(
      zip( map( keys( params ), ( v ) => `data.properties.${ v }` ), values( params ) )
    )

    return { selector }

  }
}

const databases = {
  labels: [ 'features', 'points', 'zones' ],
  async connect ( self = 'all' ) {

    const { failure, warning, success, info } = ʟᴏɢɢᴇʀ( __filename, { name: 'databases.connect' } )

    const { labels } = databases
    const { last, difference, isEqual, not, isUndefined, join } = ᴜɴɪᴛʏ

    const label = last( difference( self, difference( self, labels ) ) )

    if ( not( isUndefined( label ) ) )
      info( `Selected '${ label }'` )
    else
      info( `Selected stack 〈${ join( labels, ' 〉〈' ) } 〉` )

    const all = await ᴇɴɢɪɴᴇ( labels )
    const selected = isEqual( self, 'all' ) ? all : all[ label ]

    return selected

  }
}

const tokens = {
  features: {
    subscription: { origin: 'feature' },
    points      : { origin: 'feature' },
    zones       : { origin: 'feature', points: { origin: 'zone' } },
    origin      : 'article'
  },
  origin ( self ) {

    const { initial, get, join } = ᴜɴɪᴛʏ

    const path = initial( self )

    const { origin } = get( tokens, join( path, '.' ) )

    return { origin }

  }
}

class Toggler {

  private item

  private spot

  private togglers = { default: 'data.properties.enabled' }

  public constructor ( item, self ) {

    const { cloneDeep } = ᴜɴɪᴛʏ
    this.item = cloneDeep( item )

    this.spot = this.stick( self )

  }

  public get state () {

    const { get } = ᴜɴɪᴛʏ

    return get( this.item, this.spot )

  }

  public set state ( toggle ) {

    const { set } = ᴜɴɪᴛʏ

    set( this.item, this.spot, toggle )

  }

  public get negative () {

    const { not, get } = ᴜɴɪᴛʏ

    return not( get( this.item, this.spot ) )

  }

  public get T () {

    return this

  }

  public get flush () {

    return this.item

  }

  private stick ( self ) {

    const { initial, get, join, defaultTo } = ᴜɴɪᴛʏ

    const path = initial( self )

    return defaultTo( get( this.togglers, join( path, '.' ) ), this.togglers.default )

  }

}

const current = ( origin, record, disable = false ) => {

  const { mapKeys, pick, values } = ᴜɴɪᴛʏ

  const [ id, rev ] = values( pick( record, [ 'id', '_id', 'rev', '_rev' ] ) )

  const entity = disable ? { id, rev } : { id, rev, ...origin }

  const prefixed = mapKeys( entity, ( v, k ) => `_${ k }` )

  const normfix = {
    get ( obj, prop ) {

      const { indexOf, isEqual } = ᴜɴɪᴛʏ
      const keys = [ 'id', 'rev' ]

      const i = indexOf( keys, prop )

      if ( isEqual( i, -1 ) )
        return obj[ prop ]

      return obj[ `_${ prop }` ]

    }
  }

  return new Proxy( prefixed, normfix )

}

const ᴇɴᴠ = async( self, status ) => {

  const identor = async( base, serv, params, type ) => {

    const selected = base[ `${ type }s` ]
    const { process } = find

    return process( await selected.find( find[ type ]( params ), serv.it ), serv )

  }

  const { partial, join, identity, isUndefined, not } = ᴜɴɪᴛʏ

  const { sᴛᴀᴛᴜs, ɢᴜᴀʀᴀɴᴛᴇᴇs } = ʜᴇʟᴘᴇʀs

  // eslint-disable-next-line new-cap
  const observe = new sᴛᴀᴛᴜs( self, status )
  const { meta } = observe

  const { failure, warning, success, info, note } = ʟᴏɢɢᴇʀ( __filename, { name: join( self ) } )

  const { connect } = databases

  const base: any = await connect( self )

  const { get, post, put } = base

  const { origin } = tokens

  const [ fail, warn, suss ] = [ failure, warning, success ]
  const [ orig, serv ] = [ origin, observe ]

  const iden = partial( identor, await connect(), serv )

  const bind: any = partial( current, orig( self ) )

  const controllers = ɢᴜᴀʀᴀɴᴛᴇᴇs

  let gant: any = identity
  // eslint-disable-next-line no-bitwise
  const guarante: any = ᴜɴɪᴛʏ.get( controllers, self )

  if ( not( isUndefined( guarante ) ) )
    gant = partial( guarante, self, base, serv.it )

  return {
    get,
    reg     : post,
    put,
    database: base,
    serv,
    iden,
    gant,
    meta,
    bind,
    fail,
    warn,
    suss,
    info,
    note,
    orig
  }

}

const expose = {
  async features ( self, params, convInto, target, status = 200 ) {

    const { put, get, bind, meta, iden, serv } = await ᴇɴᴠ( self, status )

    const order = await iden( params, target )
    const { isEqual } = ᴜɴɪᴛʏ
    void ( await ( async( ...args ) => {

      const T = new Toggler( ...args )

      if ( isEqual( T.state, convInto ) )
        return void serv.status( 304 )

      T.state = convInto

      return void put( T.flush, serv.it )

    } )( order, self ) )

    const check = await iden( params, target )
    const entry = bind( check )

    const product = { ...entry, ...check, ...meta() }

    return product

  },
  points: async( ...args ) => expose.features( ...args ),
  zones : async( ...args ) => expose.features( ...args ),
  router ( self, params, convInto ) {

    const { last, difference, initial, join } = ᴜɴɪᴛʏ

    const { labels } = databases

    const label = last( difference( self, difference( self, labels ) ) )

    const order = join( initial( label ), '' )

    const product = expose[ label ]( self, params, convInto, order )

    return product

  }
} as any

const articles = {
  features: {
    async post ( body, params, transform, self ) {

      const { reg, get, bind, meta, serv, gant } = await ᴇɴᴠ( self, 201 )

      const { not } = ᴜɴɪᴛʏ

      const valid = await gant( body, params )

      if ( not( valid ) )
        return { ...meta() }

      const order = await reg( transform( { body } ), serv.it )

      const check = await get( order.id, serv.it )
      const entry = bind( order )

      const product = { ...entry, ...check, ...meta() }

      return product

    },
    async get ( body, params, transform, self ) {

      const { bind, meta, iden } = await ᴇɴᴠ( self, 200 )

      const check = await iden( params, 'feature' )

      const entry = bind( check )

      const product = { ...entry, ...check, ...meta() }

      return product

    },
    async put ( body, params, transform, self ) {

      const convertInto = true

      const product = await expose.router( self, params, convertInto )

      return product

    },
    async patch ( body, params, transform, self ) {

      const convertInto = false

      const product = await expose.router( self, params, convertInto )

      return product

    },
    subscription: {
      async get ( body, params, transform, self ) {

        const { put, iden, bind, meta, serv } = await ᴇɴᴠ( self, 200 )

        const check = await iden( params, 'feature' )

        const entry = bind( check )

        const compiled = transform( { entry, check } )

        const product = { ...entry, ...compiled, ...meta() }

        return product

      },
      async put ( body, params, transform, self ) {

        const { put, iden, bind, meta, serv } = await ᴇɴᴠ( self, 205 )
        const { last, not, merge, initial } = ᴜɴɪᴛʏ

        const absolute: any = last( initial( self ) )

        const { timeRange } = body
        if ( not( validTimeRange( timeRange, serv ) ) )
          return meta()

        const check = await iden( params, 'feature' )

        check.subscription.push( body )

        check.subscription = normalizeSubscription( check.subscription )

        const prepare = check

        const order: any = await put( prepare, serv.it )
        const entry = bind( order )

        const { relationships } = check
        const compiled = transform(
          { entry, prepare: prepare.subscription, relationships },
          check
        )

        const product = { ...entry, ...compiled, ...meta() }

        return product

      },
      async patch ( body, params, transform, self ) {

        const { put, iden, bind, meta, serv } = await ᴇɴᴠ( self, 205 )
        const { last, merge, initial, not } = ᴜɴɪᴛʏ

        const absolute: any = last( initial( self ) )

        const { timeRange } = body
        if ( not( validTimeRange( timeRange, serv ) ) )
          return meta()

        const check = await iden( params, 'feature' )

        check.subscription.push( body )

        check.subscription = normalizeSubscription( check.subscription, [ body ] )

        const prepare = check

        const order: any = await put( prepare, serv.it )
        const entry = bind( order )

        const { relationships } = check
        const compiled = transform(
          { entry, prepare: prepare.subscription, relationships },
          check
        )

        const product = { ...entry, ...compiled, ...meta() }

        return product

      }
    },
    points: {
      async post ( body, params, transform, self ) {

        const { reg, iden, bind, gant, meta, serv } = await ᴇɴᴠ( self, 201 )
        const { last, merge, initial } = ᴜɴɪᴛʏ

        const { not } = ᴜɴɪᴛʏ

        const valid = await gant( body, params )

        if ( not( valid ) )
          return { ...meta() }

        const parent = await iden( params, 'feature' )

        const compiled = transform( { body }, parent )

        const order = await reg( compiled, serv.it )

        const entry = bind( order )

        const product = merge( entry, compiled, { ...meta() } )

        return product

      },
      async get ( body, params, transform, self ) {

        const { reg, iden, bind, database, meta, serv } = await ᴇɴᴠ( self, 200 )
        const { last, merge, initial } = ᴜɴɪᴛʏ

        const order = await iden( params, 'point' )

        const entry = bind( order )

        const product = { ...entry, ...order, ...meta() }

        return product

      },
      async put ( body, params, transform, self ) {

        const { bind, meta } = await ᴇɴᴠ( self, 200 )

        const convertInto = false
        const order = await expose.router( self, params, convertInto )

        const entry = bind( order )

        const product = { ...entry, ...order, ...meta() }

        return product

      },
      async patch ( body, params, transform, self ) {

        const { bind, meta } = await ᴇɴᴠ( self, 200 )

        const convertInto = false
        const order = await expose.router( self, params, convertInto )

        const entry = bind( order )

        const product = { ...entry, ...order, ...meta() }

        return product

      }
    },
    zones: {
      async post ( body, params, transform, self ) {

        const { reg, get, bind, meta, serv, gant } = await ᴇɴᴠ( self, 201 )

        const { not } = ᴜɴɪᴛʏ

        const valid = await gant( body, params )

        if ( not( valid ) )
          return { ...meta() }

        const compiled = transform( { body }, params )

        const order = await reg( compiled, serv.it )

        const check = await get( order.id, serv.it )
        const entry = bind( order )

        const product = { ...entry, ...check, ...meta() }

        return product

      },
      async get ( body, params, transform, self ) {

        const { iden, get, bind, meta, serv } = await ᴇɴᴠ( self, 200 )

        const order = await iden( params, 'zone' )

        const entry = bind( order )

        const product = { ...entry, ...order, ...meta() }

        return product

      },
      async put ( body, params, transform, self ) {

        const { iden, get, bind, meta, serv } = await ᴇɴᴠ( self, 200 )

        const convertInto = true
        const order = await expose.router( self, params, convertInto )

        const entry = bind( order )

        const product = { ...entry, ...order, ...meta() }

        return product

      },
      async patch ( body, params, transform, self ) {

        const { iden, get, bind, meta, serv } = await ᴇɴᴠ( self, 200 )

        const convertInto = false
        const order = await expose.router( self, params, convertInto )

        const entry = bind( order )

        const product = { ...entry, ...order, ...meta() }

        return product

      },
      points: {
        async post ( body, params, transform, self ) {

          const { reg, iden, bind, gant, meta, serv } = await ᴇɴᴠ( self, 201 )

          const { not } = ᴜɴɪᴛʏ

          const valid = await gant( body, params )

          if ( not( valid ) )
            return { ...meta() }

          const parent = await iden( params, 'zone' )

          const compiled = transform( { body }, parent )

          const order = await reg( compiled, serv.it )

          const entry = bind( order )

          const product = { ...entry, ...compiled, ...meta() }

          return product

        },
        async get ( body, params, transform, self ) {

          const { reg, iden, bind, database, meta, serv } = await ᴇɴᴠ( self, 200 )
          const { last, merge, initial } = ᴜɴɪᴛʏ

          const order = await iden( params, 'point' )
          const entry = bind( order )

          const product = { ...entry, ...order, ...meta() }

          return product

        },
        async put ( body, params, transform, self ) {

          const { bind, meta } = await ᴇɴᴠ( self, 200 )

          const convertInto = true
          const order = await expose.router( self, params, convertInto )

          const entry = bind( order )

          const product = { ...entry, ...order, ...meta() }

          return product

        },
        async patch ( body, params, transform, self ) {

          const { bind, meta } = await ᴇɴᴠ( self, 200 )

          const convertInto = false
          const order = await expose.router( self, params, convertInto )

          const entry = bind( order )

          const product = { ...entry, ...order, ...meta() }

          return product

        }
      }
    }
  }
}

const ɪɴsᴛᴀɴᴄᴇ = () => {

  const { info, note, alert, error } = ʟᴏɢɢᴇʀ( __filename, ɪɴsᴛᴀɴᴄᴇ )
  const { failure, warning, success } = ʟᴏɢɢᴇʀ( __filename, ɪɴsᴛᴀɴᴄᴇ )

  const setup = {
    target : 'https://tunnel.vultus.io',
    options: {
      auth: {
        username: 'fetchserver',
        password: 'sammasomallaandra'
      }
    }
  }
  const { target, options } = setup

  //
  // Included plugins
  //

  PouchDB.plugin( pouchDebug )

  const db = new PouchDB( target, options )

  PouchDB.debug.enable( '*' )

}

// ɪɴsᴛᴀɴᴄᴇ()

// const { values, map, initial } = ᴜɴɪᴛʏ
//
// const dbs = databases.connect()
//
// map( values( dbs ), ᴛᴇʀᴍɪɴᴀᴛᴇ )

export default {
  articles,
  connect   : databases.connect,
  terminate : ᴛᴇʀᴍɪɴᴀᴛᴇ,
  normRanges: normalizeSubscription
}
