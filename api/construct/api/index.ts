import foundation from './foundation'
import routes from './routes'

import db, { ʀᴜɴɴᴇʀ } from '../database/engine'

import auth from '../authentication'

import loaders from '../../lib/loaders'

const { specification } = loaders

export const sᴘᴇᴄɪғɪᴄᴀᴛɪᴏɴ = specification
export const ғᴏᴜɴᴅᴀᴛɪᴏɴ = foundation
export const ʀᴏᴜᴛᴇs = routes
export const ᴀᴜᴛʜᴇɴᴛɪᴄᴀᴛɪᴏɴ = auth.engine
export const ᴅᴀᴛᴀʙᴀsᴇ = { ʀᴜɴɴᴇʀ, ᴀʀᴛɪᴄʟᴇs: db.articles }
