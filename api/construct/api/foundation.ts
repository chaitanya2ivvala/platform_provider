//
// Declarations
//

import { Router } from 'express'

import { join } from 'path'

//
// Assets
//

import { ʟᴏɢɢᴇʀ, ᴄᴏᴍᴘᴏɴᴇɴᴛs, ᴜɴɪᴛʏ } from '..'

const { ɪɴɪᴛɪᴀʟɪᴢᴇ, ᴄᴏᴍᴘɪʟᴇʀ } = ᴄᴏᴍᴘᴏɴᴇɴᴛs

//
//
//

const { ᴘᴀʀᴛɪᴀʟ, ᴏɴᴄᴇ } = ᴜɴɪᴛʏ.ɢʟᴏʙᴀʟ

const ᴏɴPᴀᴛʜ = ᴘᴀʀᴛɪᴀʟ( ( unity, ruleset, path ) => {

  const { at } = unity
  const [ route ] = at( ruleset[ path ], 'path.express' )

  const { ɪɴɪᴛɪᴀʟɪᴢᴇHᴇᴀᴅᴇʀs, ɪɴɪᴛɪᴀʟɪᴢᴇMᴇᴛʜᴏᴅs, ɪɴɪᴛɪᴀʟɪᴢᴇSᴄʜᴇᴍᴀs } = ɪɴɪᴛɪᴀʟɪᴢᴇ

  return {
    [ route ]: {
      responses: {
        onError: {
          header: ɪɴɪᴛɪᴀʟɪᴢᴇHᴇᴀᴅᴇʀs( ruleset, path ),
          method: ɪɴɪᴛɪᴀʟɪᴢᴇMᴇᴛʜᴏᴅs( ruleset, path ),
          schema: ɪɴɪᴛɪᴀʟɪᴢᴇSᴄʜᴇᴍᴀs( ruleset, path )
        }
      }
    }
  }

}, ᴜɴɪᴛʏ )

const ᴀssᴇᴍʙʟᴇʀ = async( ruleset ) => {

  const { info, success, note } = ʟᴏɢɢᴇʀ( __filename, { name: 'ᴀssᴇᴍʙʟᴇʀ' } )

  const {
    fill,
    map,
    size,
    zip,
    keys,
    partial,
    merge,
    at,
    flatten,
    each,
    isEqual
  } = ᴜɴɪᴛʏ

  const { inarmPair } = ᴜɴɪᴛʏ.ᴄᴜsᴛᴏᴍ

  const onPath = partial( ᴏɴPᴀᴛʜ, ruleset )

  const resolver = fill( new Array( size( ruleset ) ), onPath ),
        resolved = merge( {}, ...map( zip( resolver, keys( ruleset ) ), inarmPair ) )

  // ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

  info( 'Will compile ruleset to Express syntax' )

  // ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

  const { ᴍᴇᴛʜᴏᴅsCᴏᴍᴘɪʟᴇʀ, ʜᴇᴀᴅᴇʀsCᴏᴍᴘɪʟᴇʀ, sᴄʜᴇᴍᴀsCᴏᴍᴘɪʟᴇʀ } = ᴄᴏᴍᴘɪʟᴇʀ

  // ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

  note( 'Compiles responses: 1/3 ⤑ [ onError ]: Invalid [ Headers ]' )

  // ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

  const headers = map( resolved, ( label: any, path ) => {

    return ʜᴇᴀᴅᴇʀsCᴏᴍᴘɪʟᴇʀ( at( label, 'responses.onError.header' ), path )

  } )

  // ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

  success( 'Compiled responses: 1/3 ✓ [ onError ]: Invalid [ Headers ]' )

  // ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

  note( 'Compiles responses: 2/3 ⤑ [ onError ]: Invalid [ Methods ]' )

  // ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

  const methods = map( resolved, ( label: any, path ) => {

    return ᴍᴇᴛʜᴏᴅsCᴏᴍᴘɪʟᴇʀ( at( label, 'responses.onError.method' ), path )

  } )

  // ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

  success( 'Compiled responses: 2/3 ✓ [ onError ]: Invalid [ Methods ]' )

  // ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

  note( 'Compiles responses: 3/3 ⤑ [ onError ]: Invalid [ Schema ]' )

  // ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

  const schemas = map( resolved, ( label: any, path ) => {

    return sᴄʜᴇᴍᴀsCᴏᴍᴘɪʟᴇʀ( at( label, 'responses.onError.schema' ), path )

  } )

  // ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

  success( 'Compiled responses: 3/3 ✓ [ onError ]: Invalid [ Schema ]' )

  note( 'Will make final binding to Express::router' )

  // ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

  const addaptSyntax = flatten( [ ...headers, ...methods, ...schemas ] )

  const router = Router()
  each( addaptSyntax, ( [ method, label ] ) => {

    router[ method ]( ...label )

  } )

  // ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

  success( 'Compilation: ✓ ⤑ Handing over product' )

  // ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢ ♢

  return router

}

//
//
//

export default ᴀssᴇᴍʙʟᴇʀ
