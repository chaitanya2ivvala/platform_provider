//
// Declarations
//

import { Router } from 'express'

//
// Assets
//

import { ᴅᴀᴛᴀʙᴀsᴇ } from '../chain'

const { ɴᴀᴠɪɢᴀᴛᴏʀ } = ᴅᴀᴛᴀʙᴀsᴇ

import { ᴄᴏᴍᴘᴏɴᴇɴᴛs, ᴜɴɪᴛʏ } from '..'

const { ᴄᴏɴᴠᴇʀᴛᴇʀ } = ᴄᴏᴍᴘᴏɴᴇɴᴛs

//
//
//

const checkMethod = ( allows, method ) => {

  const { castArray, difference, isEmpty, map, leam } = ᴜɴɪᴛʏ

  const valid = isEmpty( difference( castArray( method ), allows ) )

  return valid

}

const helper = async( navi, req, res ) => {

  const { not, isUndefined } = ᴜɴɪᴛʏ
  const { method, originalUrl, path, body, params } = req

  // const { failure, warning, success } = ʟᴏɢɢᴇʀ( __filename, helper )

  const product = await navi( [ method, path, body, params ] )

  if ( isUndefined( res ) )
    return product

  res.send( product )

  return void 'Never reached'

}

const onPath = ( record ) => {

  const { get, partial, map, isEmpty, leam } = ᴜɴɪᴛʏ

  const methods = get( record, 'methods' )
  const check = partial( checkMethod, methods )

  const endpoint = get( record, 'paths.reference' )
  const prepared = partial( helper, ɴᴀᴠɪɢᴀᴛᴏʀ )

  return map( methods, ( method ) => {

    return [ method, endpoint, prepared ]

  } )

}

const ᴀssᴇᴍʙʟᴇʀ = ( source, test = false ) => {

  const { toPairs, each, flatMap, map, leam } = ᴜɴɪᴛʏ

  const { unice } = ᴄᴏɴᴠᴇʀᴛᴇʀ

  const instance = unice( source )
  const ruleset = instance()

  const routes = flatMap( ruleset, onPath )

  if ( test )
    return routes

  const router = Router()
  each( routes, ( [ method, path, func ] ) => {

    router[ method ]( path, func )

  } )

  return router

}

export default ᴀssᴇᴍʙʟᴇʀ
