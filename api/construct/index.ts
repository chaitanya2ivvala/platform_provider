import { sᴛᴀᴛᴜs, ɢᴜᴀʀᴀɴᴛᴇᴇs, ᴄᴏɴᴠᴇʀᴛᴇʀ } from '../lib/helpers'
import { ɪɴɪᴛɪᴀʟɪᴢᴇ, ᴄᴏᴍᴘɪʟᴇʀ } from '../lib/parsers'

import loader from '../@static'
import unity from '../../unity'

import ᴜᴛɪʟɪᴛɪᴇs from '../../utilities'

export const ᴄᴏᴍᴘᴏɴᴇɴᴛs = { ɪɴɪᴛɪᴀʟɪᴢᴇ, ᴄᴏᴍᴘɪʟᴇʀ, ᴄᴏɴᴠᴇʀᴛᴇʀ }
export const ʜᴇʟᴘᴇʀs = { sᴛᴀᴛᴜs, ɢᴜᴀʀᴀɴᴛᴇᴇs }

const { logger, uni, identity, getUid } = ᴜᴛɪʟɪᴛɪᴇs

export const ᴜɴɪ = uni
export const ᴜɴɪᴛʏ = unity
export const ʟᴏɢɢᴇʀ = logger
export const sᴛᴀᴛɪᴄ = loader
export const ɪᴅᴇɴᴛɪᴛʏ = identity
export const ᴜɪᴅ = getUid

export default ᴜᴛɪʟɪᴛɪᴇs
