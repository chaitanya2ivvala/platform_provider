//
// Dependencies
//

import ʏᴀᴍʟ from 'js-yaml'
import ғs from 'fs'
import ᴘᴀᴛʜ from 'path'

//
// Assets
//

import ᴜɴɪᴛʏ from '../../unity'

//
//
//

const { ᴘᴀʀᴛɪᴀʟ } = ᴜɴɪᴛʏ.ɢʟᴏʙᴀʟ

const ᴅɪʀ = __dirname
const sᴛᴀᴛɪᴄ = '@static'
const ғᴏʀᴍᴀᴛ = 'yml'

const ʟᴏᴀᴅᴇʀ = ᴘᴀʀᴛɪᴀʟ(
  ( path, fs, yaml, dir, folder, format, target ) => {

    const { resolve } = path

    const engine = ( name ) => yaml.safeLoad(
      fs.readFileSync(
        resolve( dir, '../', folder, `${ name }.${ format }` ),
        'utf8'
      )
    )

    return engine( target )

  },
  ᴘᴀᴛʜ,
  ғs,
  ʏᴀᴍʟ,
  ᴅɪʀ,
  sᴛᴀᴛɪᴄ,
  ғᴏʀᴍᴀᴛ
)

export default ʟᴏᴀᴅᴇʀ
