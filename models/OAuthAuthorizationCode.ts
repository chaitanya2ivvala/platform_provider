import bcrypt from 'bcrypt-nodejs'
import crypto from 'crypto'
import mongoose, { Document, Model } from 'mongoose'
import uid from '../utilities/uid'

const { Schema } = mongoose
declare type AuthorizationCodeSchema = {
  code: String
  userId: String
  clientId: String
  clientSecret: String
  redirectUri: String
  isConsumed: Boolean
}
const OAuthAuthorizationCodeSchema = new Schema( {
  code: {
    type  : String,
    unique: true,
    default () {

      const { getUid } = uid

      return getUid( 124 )

    }
  },
  userId      : { type: String, required: true },
  clientId    : { type: String, required: true },
  clientSecret: { type: String, required: true },
  redirectUri : { type: String },
  isConsumed  : { type: Boolean, default: false }
} )

const OAuthAuthorizationCode: Model<
Document & AuthorizationCodeSchema
> = mongoose.model( 'OAuthAuthorizationCode', OAuthAuthorizationCodeSchema )

export default OAuthAuthorizationCode
