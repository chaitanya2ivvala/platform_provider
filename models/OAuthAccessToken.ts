import mongoose, { Document, Model } from 'mongoose'
import uid from '../utilities/uid'

const { Schema, model } = mongoose
declare type AccessTokenSchema = {
  authorizationCode: String
  accessToken: String
  tokenType: String
  scope: String
  expiresIn: Date
  refreshToken: String
  userId: String
  clientId: String
  clientSecret: String

  isTrusted: Boolean
}
const OAuthAccessTokenSchema = new Schema( {
  authorizationCode: { type: String },
  accessToken      : {
    type  : String,
    unique: true,
    default () {

      const { getUid } = uid

      return getUid( 124 )

    }
  },
  tokenType: { type: String, default: 'bearer' },
  scope    : [ { type: String, default: '*' } ],
  expiresIn: {
    type: Date,
    default () {

      const today = new Date()
      const base = 1000
      const minute = 60 * base // Length (in minutes) of our access token
      const hour = 60 * minute
      const day = 24 * hour

      return new Date( today.getTime() + day * 1095 )

    }
  },
  refreshToken: {
    type: String,
    default () {

      const { getUid } = uid

      return getUid( 124 )

    }
  },
  userId      : { type: String },
  clientId    : { type: String },
  clientSecret: { type: String },
  isTrusted   : { type: Boolean, default: true }
} )

const OAuthAccessToken: Model<Document & AccessTokenSchema> = mongoose.model(
  'OAuthAccessToken',
  OAuthAccessTokenSchema
)

export default OAuthAccessToken
