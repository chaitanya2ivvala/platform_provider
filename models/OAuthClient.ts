import bcrypt from 'bcrypt-nodejs'
import crypto from 'crypto'
import mongoose, { Document, Model } from 'mongoose'
import uid from '../utilities/uid'

const { Schema } = mongoose
declare type ClientSchema = {
  name: String
  clientId: String
  clientSecret: String
  redirectUri: String
  isTrusted: Boolean
}

/*
clientID    : process.env.VULTUS_ID,
clientSecret: process.env.VULTUS_SECRET,
callbackURL : `${ process.env.AUTHENTICATION_URL }${
  process.env.VULTUS_CALLBACK
}`,

*/
const OAuthClientSchema = new Schema( {
  name        : { type: String, required: true },
  clientId    : { type: String, required: true, unique: true },
  clientSecret: {
    type  : String,
    unique: true,
    default () {

      const { getUid } = uid

      return getUid( 124 )

    }
  },
  redirectUri: { type: String },
  isTrusted  : { type: Boolean, default: true }
} )

const OAuthClient: Model<Document & ClientSchema> = mongoose.model(
  'OAuthClient',
  OAuthClientSchema
)

export default OAuthClient
