import bcrypt from 'bcrypt-nodejs'
import crypto from 'crypto'
import mongoose from 'mongoose'

declare type User = {
  email: { type: String; unique: true } | String
  password: String
  passwordResetToken: String
  passwordResetExpires: Date

  facebook: String
  twitter: String
  vultus: String
  google: String
  github: String
  instagram: String
  linkedin: String
  steam: String
  tokens: Array<any>

  profile: {
    name: String
    gender: String
    location: String
    token: String
    website: String
    picture: String
  }
}

const userSchema = new mongoose.Schema(
  {
    email               : { type: String, unique: true },
    password            : String,
    passwordResetToken  : String,
    passwordResetExpires: Date,

    facebook : String,
    twitter  : String,
    vultus   : String,
    google   : String,
    github   : String,
    instagram: String,
    linkedin : String,
    steam    : String,
    tokens   : Array,

    profile: {
      name    : String,
      gender  : String,
      location: String,
      token   : String,
      website : String,
      picture : String
    }
  },
  { timestamps: true }
)

/**
 * Password hash middleware.
 */
userSchema.pre( 'save', function save ( next ) {

  const user = this
  if ( !user.isModified( 'password' ) ) {

    return next()

  }
  bcrypt.genSalt( 10, ( error, salt ) => {

    if ( error ) {

      return next( error )

    }
    bcrypt.hash( user.password, salt, null, ( error, hash ) => {

      if ( error ) {

        return next( error )

      }
      user.password = hash

      return void next()

    } )

    return void 'ɴᴇᴠᴇʀ'

  } )

  return void 'ɴᴇᴠᴇʀ'

} )

/**
 * Helper method for validating user's password.
 */
userSchema.methods.comparePassword = function comparePassword (
  candidatePassword,
  cb
) {

  bcrypt.compare( candidatePassword, this.password, ( err, isMatch ) => {

    cb( err, isMatch )

  } )

}

/**
 * Helper method for getting user's gravatar.
 */
userSchema.methods.gravatar = function gravatar ( _size ) {

  let size = _size

  if ( !size ) {

    size = 200

  }
  if ( !this.email ) {

    return `https://gravatar.com/avatar/?s=${ size }&d=retro`

  }
  const md5 = crypto
    .createHash( 'md5' )
    .update( this.email )
    .digest( 'hex' )

  return `https://gravatar.com/avatar/${ md5 }?s=${ size }&d=retro`

}

const user: mongoose.Model<
mongoose.Document & User & { comparePassword: ( psw: any, f: any ) => {} }
> = mongoose.model( 'User', userSchema )

export default user
