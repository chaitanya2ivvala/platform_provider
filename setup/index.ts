import axios from 'axios'
import fs from 'fs'

import bluebird from 'bluebird'
import https from 'https'
import path from 'path'

// eslint-disable-next-line import/no-extraneous-dependencies
import prettier from 'prettier'
// eslint-disable-next-line import/no-extraneous-dependencies
import sort from 'sort-json'
import yaml from 'js-yaml'

import { filter, keys, map, merge, zip } from 'lodash'

const dir = __dirname
const file = 'package.json'
const registry =
  'https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/'

const regTypes = async( { dependencies, devDependencies }, to = 'bin' ) => {

  const { join } = path
  if ( !fs.existsSync( join( dir, to ) ) ) {

    fs.mkdirSync( join( dir, to ) )

  }

  const config = { validateStatus: ( status ) => true }

  const modules = [ ...keys( dependencies ), ...keys( devDependencies ) ]

  const status = await bluebird.all(
    map( modules, ( item ) => axios.head( `${ registry }${ item }`, config ) )
  )

  const grouped = zip( modules, status )
  const full = map( grouped, ( [ l, s ] ) => ( { module: l, status: s.status } ) )
  const prep = filter( full, [ 'status', 200 ] )

  const typings = merge(
    {},
    ...map( prep, ( { module } ) => ( { [ `@types/${ module }` ]: '0.0.0' } ) )
  )

  return typings

}

const packages = async() => {

  console.log( 'Compile Package' )
  const { join } = path

  const root = join( dir, 'packages' )
  const names = fs.readdirSync( root )

  // eslint-disable-next-line lodash/prefer-lodash-method
  const engine = ( name ) => yaml.safeLoad( fs.readFileSync( join( root, name ), 'utf8' ) )
  const source = merge( {}, ...map( names, engine ) )

  const prepared = JSON.parse(
    prettier.format( JSON.stringify( source ), { parser: 'json' } )
  )

  let { devDependencies } = sort( prepared, { depth: 3 } )
  const { scripts, dependencies } = sort( prepared, { depth: 3 } )

  const typings = await regTypes( { dependencies, devDependencies } )

  devDependencies = merge( typings, devDependencies )

  devDependencies = sort( devDependencies, { depth: 3 } )

  const final = {
    ...prepared,
    scripts: sort( scripts ),
    dependencies,
    devDependencies
  }

  fs.writeFileSync(
    join( dir, 'bin', file ),
    prettier.format( JSON.stringify( final ), { parser: 'json' } ),
    { flag: 'w' }
  )

  return { dependencies, devDependencies }

}

//
// 1. Install Dependencies
// npm install typescript, ts-node prettier sort-json js-yaml
// 2. Run script | ts-node ./setup
//

packages()

export default packages
