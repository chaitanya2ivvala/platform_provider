/**
 * Module dependencies.
 */

import { InternalOAuthError, OAuth2Strategy } from 'passport-oauth'
import util from 'util'

import { cloneDeep } from 'lodash'

/**
 * `Strategy` constructor.
 *
 * The example-oauth2orize authentication strategy authenticates requests by delegating to
 * example-oauth2orize using the OAuth 2.0 protocol.
 *
 * Applications must supply a `verify` callback which accepts an `accessToken`,
 * `refreshToken` and service-specific `profile`, and then calls the `done`
 * callback supplying a `user`, which should be set to `false` if the
 * credentials are not valid.  If an exception occured, `err` should be set.
 *
 * Options:
 *   - `clientID`      your example-oauth2orize application's client id
 *   - `clientSecret`  your example-oauth2orize application's client secret
 *   - `callbackURL`   URL to which example-oauth2orize will redirect the user after granting authorization
 * @param {Object} options
 * @param {Function} verify
 * @api public
 */
function Strategy ( _options, verify ) {

  const me = this

  const options = _options || {}
  options.authorizationURL = `${ process.env.AUTHORIZATION_URL }/dialog/authorize`
  options.tokenURL = `${ process.env.AUTHORIZATION_URL }/oauth/token`

  OAuth2Strategy.call( me, options, verify )

  // must be called after prototype is modified

  me.name = 'vultus'

}

/**
 * Inherit from `OAuth2Strategy`.
 */
util.inherits( Strategy, OAuth2Strategy )

/**
 * Retrieve user profile from example-oauth2orize.
 *
 * This function constructs a normalized profile, with the following properties:
 *
 *   - `provider`         always set to `example-oauth2orize`
 *   - `id`
 *   - `username`
 *   - `displayName`
 *
 * @param {String} accessToken
 * @param {Function} done
 * @api protected
 */
Strategy.prototype.userProfile = function Profile ( accessToken, done ) {

  const current = this
  const URL = `${ process.env.AUTHORIZATION_URL }${ process.env.VULTUS_PROFILE }`
  current._oauth2.get( URL, accessToken, ( err, body, res ) => {

    let json

    if ( err ) {

      if ( err.data ) {

        try {

          json = JSON.parse( err.data )

        }
        catch ( error ) {

          return void 'ɴᴏᴛʜɪɴɢ ᴛᴏ ᴘᴀss'

        }

      }

      return done( new InternalOAuthError( 'Failed to fetch user profile', err ) )

    }

    try {

      json = JSON.parse( body )

    }
    catch ( error ) {

      return done( new Error( 'Failed to parse user profile' ) )

    }

    const profile: any = cloneDeep( json )
    profile.provider = current.name
    profile._raw = body
    profile._json = json

    done( null, profile )

    return new Error( void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ sᴇᴄᴛɪᴏɴ' )

  } )

}

/**
 * Expose `Strategy`.
 */
// module.exports.Strategy = Strategy.Strategy = Strategy

export default Strategy
