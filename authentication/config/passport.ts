import passport from 'passport'
import request from 'request'

import { uniq } from 'lodash'

//
// Strategy
//

import { Strategy as FacebookStrategy } from 'passport-facebook'
import { Strategy as GitHubStrategy, StrategyOption } from 'passport-github'
import { Strategy as InstagramStrategy } from 'passport-instagram'
import { Strategy as LinkedInStrategy } from 'passport-linkedin-oauth2'
import { Strategy as LocalStrategy } from 'passport-local'
import { Strategy as OpenIDStrategy } from 'passport-openid'
import { Strategy as TwitterStrategy } from 'passport-twitter'

//
// OAuth2 Strategy
//

import { OAuth2Strategy as GoogleStrategy } from 'passport-google-oauth'

//
// Inherit same module independent from version
//

import { OAuth2Strategy, OAuthStrategy } from 'passport-oauth'

//
// Strategy Vultus
//

import VultusStrategy from '../passport/passport-vultus'

import User from '../../models/User'

passport.serializeUser( ( user, done ) => {

  // @ts-ignore

  done( null, user.id )

} )
passport.deserializeUser( ( id, done ) => {

  User.findById( id, ( error, user ) => {

    done( error, user )

  } )

} )

passport.use(
  new VultusStrategy(
    {
      clientID    : process.env.VULTUS_ID,
      clientSecret: process.env.VULTUS_SECRET,
      callbackURL : `${ process.env.AUTHENTICATION_URL }${
        process.env.VULTUS_CALLBACK
      }`,
      passReqToCallback: true
    },
    ( req, accessToken, refreshToken, profile, done ) => {

      User.findOne( { vultus: profile.id }, ( error, existingUser ) => {

        if ( error ) {

          return done( error )

        }

        if ( existingUser ) {

          return done( null, existingUser )

        }

        User.findById( profile.id, ( error, user ) => {

          if ( error ) {

            return done( error )

          }

          user.email = profile._json.email
          user.vultus = profile.id
          user.tokens.push( { kind: 'vultus', accessToken } )
          user.profile.name = profile.name
          user.profile.token = accessToken

          user.save( ( error ) => {

            done( error, user )

          } )

          return void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ'

        } )

        return void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ'

      } )

    }
  )
)

/**
 * Sign in using Email and Password.
 */
passport.use(
  new LocalStrategy( { usernameField: 'email' }, ( email, password, done ) => {

    User.findOne( { email: email.toLowerCase() }, ( error, user ) => {

      if ( error ) {

        return done( error )

      }
      if ( !user ) {

        return done( null, false, { message: `Email ${ email } not found.` } )

      }
      user.comparePassword( password, ( error, isMatch ) => {

        if ( error ) {

          return done( error )

        }
        if ( isMatch ) {

          return done( null, user )

        }

        return done( null, false, { message: 'Invalid email or password.' } )

      } )

      return void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ'

    } )

  } )
)

/**
 * OAuth Strategy Overview
 *
 * - User is already logged in.
 *   - Check if there is an existing account with a provider id.
 *     - If there is, return an erroror message. (Account merging not supported)
 *     - Else link new OAuth account with currently logged-in user.
 * - User is not logged in.
 *   - Check if it's a returning user.
 *     - If returning user, sign in and we are done.
 *     - Else check if there is an existing account with user's email.
 *       - If there is, return an erroror message.
 *       - Else create a new account.
 */

/**
 * Sign in with Facebook.
 */
passport.use(
  new FacebookStrategy(
    {
      clientID     : process.env.FACEBOOK_ID,
      clientSecret : process.env.FACEBOOK_SECRET,
      callbackURL  : `${ process.env.BASE_URL }${ process.env.FACEBOOK_CALLBACK }`,
      profileFields: [
        'name',
        'email',
        'link',
        'locale',
        'timezone',
        'gender'
      ],
      passReqToCallback: true
    },
    ( req, accessToken, refreshToken, profile, done ) => {

      if ( req.user ) {

        User.findOne( { facebook: profile.id }, ( error, existingUser ) => {

          if ( error ) {

            return done( error )

          }
          if ( existingUser ) {

            // @ts-ignore

            req.flash( 'errors', {
              message:
                'There is already a Facebook account that belongs to you. Sign in with that account or delete it, then link it with your current account.'
            } )
            done( error )

          }
          else {

            User.findById( req.user.id, ( error, user ) => {

              if ( error ) {

                return done( error )

              }
              user.facebook = profile.id
              user.tokens.push( { kind: 'facebook', accessToken } )
              user.profile.name =
                user.profile.name ||
                `${ profile.name.givenName } ${ profile.name.familyName }`
              user.profile.gender = user.profile.gender || profile._json.gender
              user.profile.picture =
                user.profile.picture ||
                `https://graph.facebook.com/${ profile.id }/picture?type=large`
              user.save( ( error ) => {

                // @ts-ignore

                req.flash( 'info', { msg: 'Facebook account has been linked.' } )
                done( error, user )

              } )

              return void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ'

            } )

            return void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ'

          }

          return void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ'

        } )

      }
      else {

        User.findOne( { facebook: profile.id }, ( error, existingUser ) => {

          if ( error ) {

            return done( error )

          }
          if ( existingUser ) {

            return done( null, existingUser )

          }
          User.findOne(
            { email: profile._json.email },
            ( error, existingEmailUser ) => {

              if ( error ) {

                return done( error )

              }
              if ( existingEmailUser ) {

                // @ts-ignore
                req.flash( 'errors', {
                  msg:
                    'There is already an account using this email address. Sign in to that account and link it with Facebook manually from Account Settings.'
                } )
                done( error )

              }
              else {

                const user = new User()
                user.email = profile._json.email
                user.facebook = profile.id
                user.tokens.push( { kind: 'facebook', accessToken } )
                user.profile.name = `${ profile.name.givenName } ${
                  profile.name.familyName
                }`
                user.profile.gender = profile._json.gender
                user.profile.picture = `https://graph.facebook.com/${
                  profile.id
                }/picture?type=large`
                user.profile.location = profile._json.location
                  ? profile._json.location.name
                  : ''
                user.save( ( error ) => {

                  done( error, user )

                } )

              }

              return void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ'

            }
          )

          return void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ'

        } )

      }

    }
  )
)

/**
 * Sign in with Google.
 */
passport.use(
  new GoogleStrategy(
    {
      clientID         : process.env.GOOGLE_ID,
      clientSecret     : process.env.GOOGLE_SECRET,
      callbackURL      : `${ process.env.BASE_URL }${ process.env.GOOGLE_CALLBACK }`,
      passReqToCallback: true
    },
    ( req, accessToken, refreshToken, profile, done ) => {

      if ( req.user ) {

        User.findOne( { google: profile.id }, ( error, existingUser ) => {

          if ( error ) {

            return done( error )

          }
          if ( existingUser ) {

            // @ts-ignore

            req.flash( 'errors', {
              msg:
                'There is already a Google account that belongs to you. Sign in with that account or delete it, then link it with your current account.'
            } )
            done( error )

          }
          else {

            User.findById( req.user.id, ( error, user ) => {

              if ( error ) {

                return done( error )

              }
              user.google = profile.id
              user.tokens.push( { kind: 'google', accessToken } )
              user.profile.name = user.profile.name || profile.displayName
              user.profile.gender = user.profile.gender || profile._json.gender
              user.profile.picture =
                user.profile.picture || profile._json.image.url
              user.save( ( error ) => {

                // @ts-ignore
                req.flash( 'info', { msg: 'Google account has been linked.' } )
                done( error, user )

                return void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ'

              } )

              return void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ'

            } )

          }

          return void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ'

        } )

      }
      else {

        User.findOne( { google: profile.id }, ( error, existingUser ) => {

          if ( error ) {

            return done( error )

          }
          if ( existingUser ) {

            return done( null, existingUser )

          }
          User.findOne(
            { email: profile.emails[ 0 ].value },
            ( error, existingEmailUser ) => {

              if ( error ) {

                return done( error )

              }
              if ( existingEmailUser ) {

                // @ts-ignore
                req.flash( 'errors', {
                  msg:
                    'There is already an account using this email address. Sign in to that account and link it with Google manually from Account Settings.'
                } )
                done( error )

              }
              else {

                const user = new User()
                user.email = profile.emails[ 0 ].value
                user.google = profile.id
                user.tokens.push( { kind: 'google', accessToken } )
                user.profile.name = profile.displayName
                user.profile.gender = profile._json.gender
                user.profile.picture = profile._json.image.url
                user.save( ( error ) => {

                  done( error, user )

                } )

              }

              return void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ'

            }
          )

          return void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ'

        } )

      }

    }
  )
)

/**
 * Sign in with LinkedIn.
 */
passport.use(
  new LinkedInStrategy(
    {
      clientID         : process.env.LINKEDIN_ID,
      clientSecret     : process.env.LINKEDIN_SECRET,
      callbackURL      : `${ process.env.BASE_URL }${ process.env.LINKEDIN_CALLBACK }`,
      scope            : [ 'r_basicprofile', 'r_emailaddress' ],
      passReqToCallback: true
    },
    ( req, accessToken, refreshToken, profile, done ) => {

      if ( req.user ) {

        User.findOne( { linkedin: profile.id }, ( error, existingUser ) => {

          if ( error ) {

            return done( error )

          }
          if ( existingUser ) {

            req.flash( 'errors', {
              msg:
                'There is already a LinkedIn account that belongs to you. Sign in with that account or delete it, then link it with your current account.'
            } )
            done( error )

          }
          else {

            User.findById( req.user.id, ( error, user ) => {

              if ( error ) {

                return done( error )

              }
              user.linkedin = profile.id
              user.tokens.push( { kind: 'linkedin', accessToken } )
              user.profile.name = user.profile.name || profile.displayName
              user.profile.location =
                user.profile.location || profile._json.location.name
              user.profile.picture =
                user.profile.picture || profile._json.pictureUrl
              user.profile.website =
                user.profile.website || profile._json.publicProfileUrl
              user.save( ( error ) => {

                if ( error ) {

                  return done( error )

                }
                req.flash( 'info', { msg: 'LinkedIn account has been linked.' } )
                done( error, user )

                return void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ'

              } )

              return void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ'

            } )

          }

          return void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ'

        } )

      }
      else {

        User.findOne( { linkedin: profile.id }, ( error, existingUser ) => {

          if ( error ) {

            return done( error )

          }
          if ( existingUser ) {

            return done( null, existingUser )

          }
          User.findOne(
            { email: profile._json.emailAddress },
            ( error, existingEmailUser ) => {

              if ( error ) {

                return done( error )

              }
              if ( existingEmailUser ) {

                req.flash( 'errors', {
                  msg:
                    'There is already an account using this email address. Sign in to that account and link it with LinkedIn manually from Account Settings.'
                } )
                done( error )

              }
              else {

                const user = new User()
                user.linkedin = profile.id
                user.tokens.push( { kind: 'linkedin', accessToken } )
                user.email = profile._json.emailAddress
                user.profile.name = profile.displayName
                user.profile.location = profile._json.location.name
                user.profile.picture = profile._json.pictureUrl
                user.profile.website = profile._json.publicProfileUrl
                user.save( ( error ) => {

                  done( error, user )

                } )

              }

              return void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ'

            }
          )

          return void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ'

        } )

      }

    }
  )
)

/**
 * Login Required middleware.
 */
const isAuthenticated = ( req, res, next ) => {

  if ( req.isAuthenticated() ) {

    return next()

  }
  res.redirect( '/login' )

  return void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ'

}

/**
 * Authorization Required middleware.
 */
const isAuthorized = ( req, res, next ) => {

  const provider = req.path.split( '/' ).slice( -1 )[ 0 ]
  const token = req.user.tokens.find( ( _token ) => _token.kind === provider )
  if ( token ) {

    next()

  }
  else {

    res.redirect( `/auth/${ provider }` )

  }

}

export default {
  isAuthenticated,
  isAuthorized
}
