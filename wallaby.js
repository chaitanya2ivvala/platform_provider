module.exports = function ( w ) {

  return {
    files: [
      '!service/components/**/*.*.ts',
      '!api/**/*.*.ts',
      '!api/gen/**/*',
      '!api/index.ts',
      'service/components/**/*',
      'api/**/*'
    ],
    tests        : [ 'api/**/*.spec.ts' ],
    env          : { type: 'node' },
    testFramework: 'ava'
  }

}
