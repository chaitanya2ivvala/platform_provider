import { ary,
  at,
  attempt,
  camelCase,
  castArray,
  clone,
  cloneDeep,
  compact,
  constant,
  create,
  curry,
  curryRight,
  defaultTo,
  defaults,
  difference,
  dropRight,
  entries,
  escapeRegExp,
  fill,
  filter,
  findIndex,
  findKey,
  flatMap,
  flatten,
  forEach,
  forOwn,
  fromPairs,
  get,
  has,
  hasIn,
  head,
  identity,
  inRange,
  indexOf,
  initial,
  isArray,
  isArrayLike,
  isArrayLikeObject,
  isBoolean,
  isDate,
  isEmpty,
  isEqual,
  isError,
  isFunction,
  isInteger,
  isMap,
  isMatch,
  isNative,
  isNil,
  isNull,
  isNumber,
  isObject,
  isPlainObject,
  isRegExp,
  isSet,
  isString,
  isSymbol,
  isUndefined,
  join,
  keys,
  last,
  lastIndexOf,
  map,
  mapKeys,
  merge,
  min,
  omit,
  omitBy,
  once,
  partial,
  pick,
  pull,
  pullAt,
  replace,
  reverse,
  round,
  set,
  size,
  split,
  sum,
  tail,
  take,
  takeRight,
  template,
  toLower,
  toNumber,
  toPairs,
  toString,
  toUpper,
  trim,
  unary,
  union,
  uniq,
  unzip,
  upperCase,
  upperFirst,
  values,
  without,
  zip } from 'lodash'

const camelMultiCase = ( list, deli: any = false ) => {

  const prepared = split( list, deli )
  const multify = join( map( prepared, ( v ) => `${ v }s` ), deli )

  return camelCase( multify )

}
const inarmPair = ( [ func, ...args ] ) => {

  return func( ...args )

}

const inarmPairs = ( list ) => {

  return merge( {}, ...map( list, inarmPair ) )

}

const smallCap = ( text ) => {

  const index: any = [
    [ /a/g, 'ᴀ' ],
    [ /b/g, 'ʙ' ],
    [ /c/g, 'ᴄ' ],
    [ /d/g, 'ᴅ' ],
    [ /e/g, 'ᴇ' ],
    [ /f/g, 'ғ' ],
    [ /g/g, 'ɢ' ],
    [ /h/g, 'ʜ' ],
    [ /i/g, 'ɪ' ],
    [ /j/g, 'ᴊ' ],
    [ /k/g, 'ᴋ' ],
    [ /l/g, 'ʟ' ],
    [ /m/g, 'ᴍ' ],
    [ /n/g, 'ɴ' ],
    [ /o/g, 'ᴏ' ],
    [ /p/g, 'ᴘ' ],
    [ /q/g, 'ǫ' ],
    [ /r/g, 'ʀ' ],
    [ /s/g, 's' ],
    [ /t/g, 'ᴛ' ],
    [ /u/g, 'ᴜ' ],
    [ /v/g, 'ᴠ' ],
    [ /w/g, 'ᴡ' ],
    [ /x/g, 'x' ],
    [ /y/g, 'ʏ' ],
    [ /z/g, 'ᴢ' ]
  ]
  const adapter = ( [ a, b ] ): any => {

    // eslint-disable-next-line no-param-reassign
    text = replace( text, a, b )

  }
  forEach( index, adapter )

  return text

}

const initialize = ( func ) => func()

const createInstance = ( label, ...targets ) => {

  if ( !isEmpty( without( map( targets, isObject ), true ) ) )
    throw new Error( 'Unity/createInstance: Invalid argument' )
  if ( !isEqual( escapeRegExp( label ), label ) )
    throw new Error( 'Unity/createInstance: Argument contain invalid characters' )

  // eslint-disable-next-line no-new-func
  const Instance = new Function( `return function ${ label } () {}` )()

  merge( Instance.prototype, ...map( targets, initialize ) )

  return Instance

}

const determine = ( val, type ) => {

  const build: any = partial( ( v, d ) => join( d, v ), '.' )

  const pro = 'prototype'
  const con = 'constructor'

  const notConstruct = !has( type, build( [ pro, con ] ) )

  const isConstruct = has( val, build( [ pro, con ] ) )

  let isInstance = notConstruct

  if ( !notConstruct )
    isInstance = val instanceof type

  return {
    relative : !isEmpty( without( [ isConstruct, isInstance ], false ) ),
    construct: isConstruct,
    instance : isInstance
  }

}

const relativeOf = ( val, type ) => determine( val, type ).relative
const isInstance = ( val, type ) => determine( val, type ).instance
const isConstruct = ( val, type ) => determine( val, type ).construct

const not = ( bool ) => !bool
const hover = ( data ) => {

  return {
    label ( label ) {

      return {
        [ label ]: data,
        ...data
      }

    }
  }

}

const dive = ( data ) => {

  return {
    after ( label ) {

      return merge(
        {},
        ...map( data, ( val, key ) => {

          const target = val[ label ]

          if ( isUndefined( target ) )
            return {}

          return { [ key ]: { [ label ]: val[ label ] } }

        } )
      )

    }
  }

}

const asTrue = ( val ) => true
const asFalse = ( val ) => false

const isTrue = ( val ) => isEqual( val, true )
const isFalse = ( val ) => isEqual( val, false )

const isPair = ( val ) => {

  if ( !isArray( val ) )
    return false

  if ( isEqual( size( val ), 2 ) )
    return true

  return false

}

const isPairs = ( vals ) => {

  if ( !isArray( vals ) )
    return false

  const test = map( vals, isPair )

  return isEqual( without( test, false ), test )

}

const leam = ( f ) => {

  const first = ( fir ) => {

    const second = ( sec ) => {

      return f( sec, fir )

    }

    return second

  }

  return first

}

const feam = ( f ) => {

  const first = ( fir ) => {

    const second = ( sec ) => {

      return f( fir, sec )

    }

    return second

  }

  return first

}

const hasSome = ( data, val ) => {

  if ( not( isArray( data ) ) )
    return data === val

  return not( isEqual( without( data, val ), data ) )

}

const hasJust = ( data, val ) => {

  if ( not( isArray( data ) ) )
    return data === val

  return isEmpty( without( data, val ) )

}

function normRecursive ( val, _norm ) {

  let data = map( val, ( record ) => {

    return isArray( record ) ? _norm( record ) : record

  } )
  const test = map( data, ( record ) => {

    return isArray( record ) && size( record ) > 1

  } )
  if ( hasSome( test, true ) && !hasJust( test, true ) )
    data = map( data, castArray )

  return data

}

function Nothing () {}

const Maybe = ( val ) => {

  if ( isEmpty( val ) )
    return undefined

  return val

}

function normalize ( ...val ) {

  let data = val
  // eslint-disable-next-line prefer-rest-params
  const args = arguments
  if ( isEqual( size( args ), 1 ) )
    data = head( args )

  // Array elements may contain just type Array or Other

  if ( isArray( data ) )
    data = normRecursive( data, normalize )

  if ( not( isArray( data ) ) )
    return Maybe( data )

  if ( isEqual( size( data ), 1 ) )
    return Maybe( normalize( head( data ) ) )

  return Maybe( data )

}

const prefix = ( v, pre ) => {

  return `${ pre }${ v }`

}

const notEqual = ( a, b ) => not( isEqual( a, b ) )

const canFill = ( val, reg ) => {


  // Don't allow partial execution

  if ( !hasJust( map( [ val, reg ], isUndefined ), false ) )
    return undefined

  if ( not( isString( val ) ) )
    throw new SyntaxError(
      `Unity: Expected data of type String, found ${ typeof val }`
    )

  const ref = normalize( compact( val.match( reg ) ) )

  return isEqual( ref, val )

}

const typeOf = ( val ) => typeof val

const prettiest = ( base, path, labels ) => {

  const sub = get( base, path )
  const sorted = pick( sub, labels )
  const product = set( base, path, sorted )

  return product

}

export default {
  at,
  takeRight,
  compact,
  hasSome,
  hasJust,
  dropRight,
  flatMap,
  findIndex,
  isPlainObject,
  get,
  toUpper,
  prettiest,
  canFill,
  normalize,
  typeOf,
  norm       : normalize,
  attempt,
  dive,
  fromPairs,
  notEqual,
  isArray,
  toLower,
  omit,
  defaultTo,
  isArrayLikeObject,
  isArrayLike,
  leam,
  feam,
  isBoolean,
  isPairs,
  isDate,
  prefix,
  take,
  hover,
  isEmpty,
  isNil,
  min,
  once,
  template,
  not,
  indexOf,
  isEqual,
  upperFirst,
  curryRight,
  defaults,
  escapeRegExp,
  pull,
  values,
  isError,
  sum,
  forEach,
  each       : forEach,
  isFunction,
  entries,
  isInteger,
  isTrue,
  isFalse,
  isMap,
  isMatch,
  isNative,
  hasIn,
  isPair,
  mapKeys,
  isNull,
  isNumber,
  isObject,
  inRange,
  isRegExp,
  reverse,
  forOwn,
  isSet,
  union,
  isString,
  initial,
  isSymbol,
  join,
  map,
  merge,
  partial,
  round,
  size,
  split,
  curry,
  asFalse,
  last,
  zip,
  unzip,
  uniq,
  filter,
  ary,
  head,
  clone,
  cloneDeep,
  identity,
  create,
  pullAt,
  flatten,
  inarmPair,
  inarmPairs,
  castArray,
  toPairs,
  replace    : replace as any,
  without,
  trim,
  set,
  keys,
  difference,
  upperCase,
  toNumber,
  findKey,
  isUndefined,
  has,
  unary,
  toString,
  pick,
  tail,
  omitBy,
  camelCase,
  asTrue,
  lastIndexOf,
  fill,
  ᴇxᴘʀɪᴍᴇɴᴛᴀʟ: {
    relativeOf,
    isInstance,
    isConstruct
  },
  ᴄᴜsᴛᴏᴍ: {
    camelMultiCase,
    inarmPair,
    inarmPairs,
    initialize,
    smallCap,
    createInstance
  },
  ᴄᴏɴᴠᴇʀᴛ: {},
  ɢʟᴏʙᴀʟ : {
    ᴘᴀʀᴛɪᴀʟ: partial,
    ᴏɴᴄᴇ   : once
  }
}
