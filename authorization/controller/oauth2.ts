// const oauth2orize = require('oauth2orize');
// const passport = require('passport');
// const login = require('connect-ensure-login');
// const db = require('../db');
// const utils = require('../utils');

import login from 'connect-ensure-login'
import oauth2orize from 'oauth2orize'
import passport from 'passport'

import utils from '../../utilities'

import Client from '../../models/OAuthClient'
import Code from '../../models/OAuthAuthorizationCode'
import Token from '../../models/OAuthAccessToken'
import User from '../../models/User'

// Create OAuth 2.0 server

const server = oauth2orize.createServer()

// Register serialialization and deserialization functions.
//
// When a client redirects a user to user authorization endpoint, an
// authorization transaction is initiated. To complete the transaction, the
// user must authenticate and approve the authorization request. Because this
// may involve multiple HTTP request/response exchanges, the transaction is
// stored in the session.
//
// An application must supply serialization functions, which determine how the
// client object is serialized into the session. Typically this will be a
// simple matter of serializing the client's ID, and deserializing by finding
// the client by ID from the database.

server.serializeClient( ( client, done ) => done( null, client.id ) )

server.deserializeClient( ( id, done ) => {

  Client.findById( id, ( error, client ) => {

    if ( error )
      return done( error )

    return done( null, client )

  } )

} )

// Register supported grant types.
//
// OAuth 2.0 specifies a framework that allows users to grant client
// applications limited access to their protected resources. It does this
// through a process of the user granting access, and the client exchanging
// the grant for an access token.

// Grant authorization codes. The callback takes the `client` requesting
// authorization, the `redirectUri` (which is used as a verifier in the
// subsequent exchange), the authenticated `user` granting access, and
// their response, which contains approved scope, duration, etc. as parsed by
// the application. The application issues a code, which is bound to these
// values, and will be exchanged for an access token.

server.grant(
  oauth2orize.grant.code( ( client, redirectUri, user, ares, done ) => {

    const code = utils.getUid( 16 )

    const [ userId ] = [ user.id ]

    const { clientId, clientSecret } = client

    Code.findOne( { clientId, userId }, ( error, authCode ) => {

      if ( authCode !== null )
        if ( authCode.isConsumed && authCode.redirectUri !== redirectUri )
          return done( null, false as any )

      if ( authCode === null ) {

        new Code( {
          code,
          clientId,
          clientSecret,
          userId,
          redirectUri
        } ).save( ( error ) => {

          if ( error )
            return done( error )

          return done( null, code )

        } )

      }
      else {

        return done( null, authCode.code as any )

      }

      return new Error( void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ sᴇᴄᴛɪᴏɴ' )

    } )

  } )
)

// Grant implicit authorization. The callback takes the `client` requesting
// authorization, the authenticated `user` granting access, and
// their response, which contains approved scope, duration, etc. as parsed by
// the application. The application issues a token, which is bound to these
// values.

server.grant(
  oauth2orize.grant.token( ( client, user, ares, done ) => {

    const accessToken = utils.getUid( 256 )

    const [ clientId, userId ] = [ client.id, user.id ]

    Token.findOne( { clientId, userId }, ( error, authToken ) => {

      const newToken = new Token( {
        ...authToken,
        accessToken,
        clientId,
        userId
      } )

      newToken.save( ( error ) => {

        if ( error )
          return done( error )

        return done( null, accessToken )

      } )

    } )

  } )
)

// Exchange authorization codes for access tokens. The callback accepts the
// `client`, which is exchanging `code` and any `redirectUri` from the
// authorization request for verification. If these values are validated, the
// application issues an access token on behalf of the user who authorized the
// code.

server.exchange(
  oauth2orize.exchange.code( ( client, code, redirectUri, done ) => {

    Code.findOne( { code }, ( error, authCode ) => {

      if ( error )
        return done( error )
      if ( client.clientId !== authCode.clientId )
        return done( null, false )
      if ( redirectUri !== authCode.redirectUri )
        return done( null, false )
      if ( authCode.isConsumed )
        return done( null, false )

      // ADD ISCONSUMED

      const accessToken = utils.getUid( 256 )

      const authorizationCode = authCode.code

      Token.findOne( { authorizationCode }, ( error, authToken ) => {

        const { userId, clientId, clientSecret } = authCode

        if ( authToken === null ) {

          const newToken = new Token( {
            userId,
            clientId,
            clientSecret,
            accessToken,
            authorizationCode
          } )
          newToken.save( ( error ) => {

            if ( error )
              return done( error )

            return done( null, accessToken )

          } )

        }
        else {

          return done( null, authToken.accessToken as any )

        }

        return new Error( void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ sᴇᴄᴛɪᴏɴ' )

      } )

      return new Error( void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ sᴇᴄᴛɪᴏɴ' )

    } )

  } )
)

// Exchange user id and password for access tokens. The callback accepts the
// `client`, which is exchanging the user's name and password from the
// authorization request for verification. If these values are validated, the
// application issues an access token on behalf of the user who authorized the code.

server.exchange(
  oauth2orize.exchange.password( ( client, username, password, scope, done ) => {


    // Validate the client

    const { clientId } = client
    Client.findOne( { clientId }, ( error, localClient ) => {

      if ( error )
        return done( error )
      if ( !localClient )
        return done( null, false )
      if ( localClient.clientSecret !== client.clientSecret )
        return done( null, false )

      // Validate the user

      User.findOne( { email: username.toLowerCase() }, ( error, user ) => {

        if ( error )
          return done( error )
        if ( !user )
          return done( null, false )
        if ( password !== user.password )
          return done( null, false )

        // Everything validated, return the token

        const accessToken = utils.getUid( 256 )

        const [ userId ] = [ user.id ]

        Token.findOne( { clientId, userId }, ( error, authToken ) => {

          const newToken = new Token( {
            ...authToken,
            accessToken,
            clientId,
            userId
          } )

          newToken.save( ( error ) => {

            if ( error )
              return done( error )

            return done( null, accessToken )

          } )

        } )

        return new Error( void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ sᴇᴄᴛɪᴏɴ' )

      } )

      return new Error( void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ sᴇᴄᴛɪᴏɴ' )

    } )

  } )
)

// Exchange the client id and password/secret for an access token. The callback accepts the
// `client`, which is exchanging the client's id and password/secret from the
// authorization request for verification. If these values are validated, the
// application issues an access token on behalf of the client who authorized the code.

server.exchange(
  oauth2orize.exchange.clientCredentials( ( client, scope, done ) => {

    // Validate the client

    const { clientId } = client
    Client.findOne( { clientId }, ( error, localClient ) => {

      if ( error )
        return done( error )
      if ( !localClient )
        return done( null, false )
      if ( localClient.clientSecret !== client.clientSecret )
        return done( null, false )

      // Everything validated, return the token

      const { clientSecret } = localClient
      const accessToken = utils.getUid( 256 )

      // Pass in a null for user id since there is no user with this grant type

      Token.findOne( { accessToken }, ( error, authToken ) => {

        const newToken = new Token( {
          ...authToken,
          clientId,
          clientSecret,
          accessToken
        } )
        newToken.save( ( error ) => {

          if ( error )
            return done( error )

          return done( null, accessToken )

        } )

      } )

      return new Error( void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ sᴇᴄᴛɪᴏɴ' )

    } )

  } )
)

// User authorization endpoint.
//
// `authorization` middleware accepts a `validate` callback which is
// responsible for validating the client making the authorization request. In
// doing so, is recommended that the `redirectUri` be checked against a
// registered value, although security requirements may vary accross
// implementations. Once validated, the `done` callback must be invoked with
// a `client` instance, as well as the `redirectUri` to which the user will be
// redirected after an authorization decision is obtained.
//
// This middleware simply initializes a new authorization transaction. It is
// the application's responsibility to authenticate the user and render a dialog
// to obtain their approval (displaying details about the client requesting
// authorization). We accomplish that here by routing through `ensureLoggedIn()`
// first, and rendering the `dialog` view.

const vultusClient = () => ( {
  name        : 'Vultus AB',
  clientId    : process.env.VULTUS_ID,
  clientSecret: process.env.VULTUS_SECRET,
  redirectUri : `${ process.env.AUTHENTICATION_URL }${ process.env.VULTUS_CALLBACK }`
} )

const authorization = [ login.ensureLoggedIn(), server.authorization(
  ( clientId, redirectUri, done ) => {

    Client.findOne( { clientId }, ( error, client: any ) => {

      const isVultus =
          client === null ? process.env.VULTUS_ID === clientId : false

      if ( isVultus ) {

        const newClient = new Client( { clientId, ...vultusClient() } )

        newClient.save( ( error ) => {

          if ( error )
            return done( error )

          return void 'ɴᴏᴛʜɪɴɢ ᴛᴏ ᴘᴀss'

        } )

        // eslint-disable-next-line no-param-reassign
        client = newClient

      }

      if ( !isVultus && error )
        return done( error )

      // WARNING: For security purposes, it is highly advisable to check that
      //          redirectUri provided by the client matches one registered with
      //          the server. For simplicity, this example does not. You have
      //          been warned.

      return done( null, client, redirectUri )

    } )

  },
  ( client, user, done: any ) => {


    // Check if grant request qualifies for immediate approval

    // Auto-approve

    if ( client.isTrusted )
      return done( null, true )
    const [ userId, clientId ] = [ user.id, client.clientId ]
    Token.findOne( { userId, clientId }, ( error, token ) => {


      // Auto-approve

      if ( token )
        return done( null, true )

      // Otherwise ask user

      return done( null, false )

    } )

    return new Error( void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ sᴇᴄᴛɪᴏɴ' )

  }
), ( request, response ) => {

  response.render( 'dialog', {
    transactionId: request.oauth2.transactionID,
    user         : request.user,
    client       : request.oauth2.client
  } )

} ]

// User decision endpoint.
//
// `decision` middleware processes a user's decision to allow or deny access
// requested by a client application. Based on the grant type requested by the
// client, the above grant middleware configured above will be invoked to send
// a response.
// @ts-ignore
const decision = [ login.ensureLoggedIn(), server.decision() ]

// Token endpoint.
//
// `token` middleware handles client requests to exchange authorization grants
// for access tokens. Based on the grant type being exchanged, the above
// exchange middleware will be invoked to handle the request. Clients must
// authenticate when making requests to this endpoint.

const token = [ passport.authenticate( [ 'basic', 'oauth2-client-password' ], { session: false } ), server.token(), server.errorHandler() ]

export default {
  authorization,
  decision,
  token
}
