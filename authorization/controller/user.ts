// const passport = require('passport');

import passport from 'passport'

const info = [ passport.authenticate( 'bearer', { session: false } ), ( request, response ) => {

  const { scope } = request.authInfo
  const { id, profile, email } = request.user
  const { name } = profile

  response.json( {
    id,
    name,
    email,
    scope
  } )

} ]

export default { info }
