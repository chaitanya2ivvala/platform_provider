// const passport = require('passport');
// const login = require('connect-ensure-login');

import ensureLogin from 'connect-ensure-login'
import passport from 'passport'

const index = ( request, response ) => response.send( 'Vultus authorization server' )

const login = passport.authenticate( 'local', {
  successReturnToOrRedirect: '/',
  failureRedirect          : '/login'
} )

/**
 * POST /login
 * Sign in using email and password.
 */
const postLogin = ( req, res, next ) => {

  req.assert( 'email', 'Email is not valid' ).isEmail()
  req.assert( 'password', 'Password cannot be blank' ).notEmpty()
  req.sanitize( 'email' ).normalizeEmail( { gmail_remove_dots: false } )

  const errors = req.validationErrors()

  if ( errors ) {

    req.flash( 'errors', errors )

    return res.redirect( '/login' )

  }

  passport.authenticate( 'local', ( error, user, info ) => {

    if ( error ) {

      return next( error )

    }
    if ( !user ) {

      req.flash( 'errors', info )

      return res.redirect( '/login' )

    }
    req.logIn( user, ( error ) => {

      if ( error ) {

        return next( error )

      }
      req.flash( 'success', { msg: 'Success! You are logged in.' } )

      res.redirect( req.session.returnTo || '/' )

      return new Error( void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ sᴇᴄᴛɪᴏɴ' )

    } )

    return new Error( void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ sᴇᴄᴛɪᴏɴ' )

  } )( req, res, next )

  return new Error( void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ sᴇᴄᴛɪᴏɴ' )

}

/**
 * GET /login
 * Login page.
 */
const getLogin = ( req, res ) => {

  if ( req.user ) {

    return res.redirect( '/' )

  }
  res.render( 'account/login', { title: 'Login' } )

  return new Error( void 'ᴜɴʀᴇᴀᴄʜᴀʙʟᴇ sᴇᴄᴛɪᴏɴ' )

}

const logout = ( request, response ) => {

  request.logout()
  response.redirect( '/' )

}

const account = [ ensureLogin.ensureLoggedIn(), ( request, response ) => response.render( 'account', { user: request.user } ) ]

export default {
  index,
  getLogin,
  login,
  logout,
  postLogin,
  account
}
