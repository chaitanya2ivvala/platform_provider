// const site = require('./site');
// const oauth2 = require('./oauth2');
// const user = require('./user');
// const client = require('./client');

import client from './client'
import oauth2 from './oauth2'
import site from './site'
import user from './user'

export default {
  site,
  oauth2,
  user,
  client
}
