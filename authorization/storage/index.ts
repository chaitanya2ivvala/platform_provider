import accessTokens from './access_tokens'
import authorizationCodes from './authorization_codes'
import clients from './clients'
import users from './users'

export default {
  users,
  clients,
  accessTokens,
  authorizationCodes
}
