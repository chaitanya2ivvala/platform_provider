const clients = [ {
  id          : '3',
  name        : 'Vultus Platform',
  clientId    : 'ward-steward-2',
  clientSecret: 'something truly secret',
  isTrusted   : true
} ]

const findById = ( id, done ) => {

  for ( let i = 0, len = clients.length; i < len; i += 1 ) {

    if ( clients[ i ].id === id )
      return done( null, clients[ i ] )

  }

  return done( new Error( 'Client Not Found' ) )

}

const findByClientId = ( clientId, done ) => {

  for ( let i = 0, len = clients.length; i < len; i += 1 ) {

    if ( clients[ i ].clientId === clientId )
      return done( null, clients[ i ] )

  }

  return done( new Error( 'Client Not Found' ) )

}

export default {
  findById,
  findByClientId
}
