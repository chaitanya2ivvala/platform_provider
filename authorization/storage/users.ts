const users = [ { id: '1', username: 'bob@vultus.io', password: 'secret', name: 'Bob Smith' }, { id: '2', username: 'joe', password: 'password', name: 'Joe Davis' } ]

const findById = ( id, done ) => {

  for ( let i = 0, len = users.length; i < len; i += 1 ) {

    if ( users[ i ].id === id )
      return done( null, users[ i ] )

  }

  return done( new Error( 'User Not Found' ) )

}

const findByUsername = ( username, done ) => {

  for ( let i = 0, len = users.length; i < len; i += 1 ) {

    if ( users[ i ].username === username )
      return done( null, users[ i ] )

  }

  return done( new Error( 'User Not Found' ) )

}

export default {
  findById,
  findByUsername
}
