// const express = require('express');
// const cookieParser = require('cookie-parser');
// const bodyParser = require('body-parser');
// const errorHandler = require('errorhandler');
// const session = require('express-session');
// const passport = require('passport');
// const routes = require('./routes');

import ConnectMongo from 'connect-mongo'
import bodyParser from 'body-parser'
import chalk from 'chalk'
import cookieParser from 'cookie-parser'
import dotenv from 'dotenv'
import errorHandler from 'errorhandler'
import express from 'express'
import expressValidator from 'express-validator'
import flash from 'express-flash'
import lusca from 'lusca'
import mongoose from 'mongoose'
import passport from 'passport'
import path from 'path'
import session from 'express-session'

//
// Authentication
//

import './auth'
import routes from './controller'

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
dotenv.load( { path: '.env.example' } )

// Express configuration

const app = express()
app.set( 'port', 3010 )
app.set( 'views', path.join( __dirname, 'views' ) )
app.set( 'view engine', 'pug' )

/**
 * Connect to MongoDB.
 */
const MongoStore = ConnectMongo( session )

mongoose.set( 'useFindAndModify', false )
mongoose.set( 'useCreateIndex', true )
mongoose.set( 'useNewUrlParser', true )
mongoose.connect( process.env.MONGODB_URI )
mongoose.connection.on( 'error', ( err ) => {

  console.error( err )
  console.log(
    '%s MongoDB connection error. Please make sure MongoDB is running.',
    chalk.red( '✗' )
  )

  // process.exit()

  throw new Error(
    'MongoDB connection error. Please make sure MongoDB is running.'
  )

} )

app.use(
  session( {
    resave           : true,
    saveUninitialized: true,
    secret           : process.env.SESSION_SECRET,
    cookie           : { maxAge: 1209600000 }, // two weeks in milliseconds
    store            : new MongoStore( {
      url          : process.env.MONGODB_URI,
      autoReconnect: true
    } )
  } )
)

app.use( cookieParser() )
app.use( bodyParser.json( { extended: false } as any ) )
app.use( bodyParser.urlencoded( { extended: false } ) )
app.use( errorHandler() )
app.use( expressValidator() )
app.use( flash() )
app.use(
  session( { secret: 'keyboard cat', resave: false, saveUninitialized: false } )
)
app.use( passport.initialize() )
app.use( passport.session() )

app.use(
  '/',
  express.static( path.join( __dirname, 'public' ), { maxAge: 31557600000 } )
)
app.use(
  '/js/lib',
  express.static( path.join( __dirname, '../node_modules/popper.js/dist/umd' ), { maxAge: 31557600000 } )
)
app.use(
  '/js/lib',
  express.static( path.join( __dirname, '../node_modules/bootstrap/dist/js' ), { maxAge: 31557600000 } )
)
app.use(
  '/js/lib',
  express.static( path.join( __dirname, '../node_modules/jquery/dist' ), { maxAge: 31557600000 } )
)
app.use(
  '/webfonts',
  express.static(
    path.join(
      __dirname,
      '../node_modules/@fortawesome/fontawesome-free/webfonts'
    ),
    { maxAge: 31557600000 }
  )
)
app.use( lusca.xframe( 'SAMEORIGIN' ) )
app.use( lusca.xssProtection( true ) )
app.disable( 'x-powered-by' )

// Passport configuration

app.get( '/', routes.site.index )
app.get( '/login', routes.site.getLogin )

// app.post( '/login', routes.site.login )

app.post( '/login', routes.site.postLogin )

app.get( '/logout', routes.site.logout )
app.get( '/account', routes.site.account )

app.get( '/dialog/authorize', routes.oauth2.authorization )
app.post( '/dialog/authorize/decision', routes.oauth2.decision )
app.post( '/oauth/token', routes.oauth2.token )

app.get( '/api/userinfo', routes.user.info )
app.get( '/api/clientinfo', routes.client.info )

app.listen( app.get( 'port' ), () => {

  console.log(
    '%s App is running at http://localhost:%d in %s mode',
    chalk.green( '✓' ),
    app.get( 'port' ),
    app.get( 'env' )
  )
  console.log( '  Press CTRL-C to stop\n' )

} )
