import express from './express'
import identity from './identity'
import logger from './logger'
import uid from './uid'
import uni from './uni'

export const ᴇxᴘʀᴇss = express
export const ʟᴏɢɢᴇʀ = logger
export const ɪᴅᴇɴᴛɪᴛʏ = identity
export const ᴜɴɪ = uni

export default { ...uid, logger, express, uni, identity }
