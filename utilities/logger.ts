//
// Node dependencies
//

import path from 'path'
import winston from 'winston'

//
// Assets
//

import ᴜɴɪᴛʏ from '../unity'

//
//
//

const ᴀᴘᴘEɴᴛʀɪᴇs = [ 'api', 'service', 'browser' ]

const { File } = winston.transports

const logger = winston.createLogger( {
  level     : 'info',
  format    : winston.format.json(),
  transports: [ new File( {
    filename: 'error.log',
    level   : 'error'
  } ), new File( { filename: 'combined.log' } ) ]
} )

const ᴘʀᴇғɪx = ( filename, { name } ) => {

  const {
    map,
    without,
    lastIndexOf,
    partial,
    min,
    difference,
    take,
    dropRight,
    join
  } = ᴜɴɪᴛʏ

  const { smallCap } = ᴜɴɪᴛʏ.ᴄᴜsᴛᴏᴍ

  const sliced = filename.split( path.sep )

  const root = partial( lastIndexOf, sliced )
  const index = min( without( map( ᴀᴘᴘEɴᴛʀɪᴇs, ( v ) => root( v ) ), -1 ) )
  const listed: any = difference( sliced, take( sliced, index ) )

  const { basename, resolve, extname } = path

  const pathed = resolve( ...listed )
  const file = basename( pathed, extname( pathed ) )
  const dir = dropRight( listed )

  const names = {
    loc: smallCap( join( dir, '/' ) ),
    fil: smallCap( file ),
    fun: smallCap( name )
  }

  // prettier-ignore

  const formated = ( ( { loc, fil, fun } ) => {

    // eslint-disable-next-line array-element-newline, array-bracket-newline // 🗁 Human〉Arm ⤑ Engine
    const list = [
      ' 🗁  ',
      join( [ loc, fil ], '/' ),
      ' ⤑ ',
      'ƒ',
      ' 〈',
      'λ ',
      fun,
      ' 〉',
      '⊱'
    ]

    return join( list, '' ) // ƒ ᴜɴɪᴛʏ〉 ᴍᴇᴛʜᴏᴅs〉 ᴍᴀᴛʜ ⤑  λ ᴀʟɢᴇʙʀᴀ

  } )( names )

  // prettier-ignore α β γ δ ε

  const technical = ( ( { loc, fil, fun } ) => {

    const date = new Date().toISOString()

    // eslint-disable-next-line array-element-newline, array-bracket-newline
    const list = [
      '|',
      date,
      '|',
      join( [ join( [ '/', loc, fil ], '/' ), fun ], '::' ),
      '|'
    ]

    return join( list, ' ' )

  } )( names )

  // formated
  // technical

  return {
    formated,
    technical
  }

}
declare type Log = ( ...args: Array<string | Error | any> ) => {}
const assembler = (
  prefix = {
    formated : '',
    technical: ''
  }
): {
throw: Log
alert: Log
error: Log
failure: Log
warning: Log
success: Log
evil: Log
info: Log
note: Log
} => {

  const { partial } = ᴜɴɪᴛʏ

  const levels = {
    emerg  : 0,
    alert  : 1,
    crit   : 2,
    error  : 3,
    warning: 4,
    notice : 5,
    info   : 6,
    debug  : 7
  }

  const bind = partial(
    ( log, lvls, type ) => {

      return log[ type ]

    },
    logger,
    levels
  )

  const error = new Error()

  const types = {
    throw  : [ '⛔️', Error, 'error' ],
    alert  : [ '⭕️', Error, 'error' ],
    error  : [ '🚫', console, 'error' ],
    failure: [ '❌', console, 'warn' ],
    warning: [ '⚠️', console, 'warn' ],
    success: [ '✔️', console, 'warn' ],
    evil   : [ '⚠', console, 'info' ],
    info   : [ '🛈', console, 'info' ],
    note   : [ '✳', console, 'debug' ]
  }

  const { isConstruct, isInstance, relativeOf } = ᴜɴɪᴛʏ.ᴇxᴘʀɪᴍᴇɴᴛᴀʟ
  const { merge, map, last, compact, join, values, each } = ᴜɴɪᴛʏ

  return merge(
    {},
    ...map( types, ( [ symbol, log, helper ], key ) => {

      const deli = ' ⋮'

      return {
        [ key ] ( ...args ): string {

          const maybe = last( args )

          //
          // Select last element of arguments
          //  IF isError
          //  o Override helper function
          //  x Select helper function
          //

          const Execute = relativeOf( maybe, Error ) ? args.pop() : log,
                entries = compact( [ ...args, Execute.message ] )

          const { formated, technical } = prefix
          const archive = bind( helper )
          archive( technical, ...entries )

          let val = join( [ symbol, deli, formated, ...entries ], '' )

          // if ( relativeOf( Execute, console ) )

          if ( isConstruct( Execute, Error ) )
            val = new Execute( join( [ ...entries ], ' ' ) )
          else
            Execute[ helper as any ]( symbol, deli, formated, ...entries )

          return val

        }
      }

    } )
  )

}

export default ( file, func ) => {

  const { isAbsolute } = path

  if ( !isAbsolute( file ) )
    throw new Error( 'Logger: Expected absolute file path' )

  return assembler( ᴘʀᴇғɪx( file, func ) )

}
