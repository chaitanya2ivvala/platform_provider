//
// Dependencies
//

import pretty from 'json-stringify-pretty-compact'

//
// Assets
//

import ʟᴏɢɢᴇʀ from './logger'
import ᴜɴɪᴛʏ from '../unity'

//
//
//

const { ᴘᴀʀᴛɪᴀʟ } = ᴜɴɪᴛʏ.ɢʟᴏʙᴀʟ

const Helpers = {
  isToken ( v ) {

    const { has } = ᴜɴɪᴛʏ

    return has( v, 'token' )

  },
  asToken ( [ a, b ] ) {

    const { isArray, size, map, pull, isEmpty, isPair } = ᴜɴɪᴛʏ
    const data = b.token

    if ( !isArray( data ) )
      return { token: a( data ) }

    // if ( isEmpty( pull( map( data, isFunction ), true ) ) )
    //   return { token: a( data ) }

    if ( isPair( data ) )
      return { token: a( ...data ) }

    return { token: a( data ) }

  },

  fromToken ( λ ) {

    const { alert } = ʟᴏɢɢᴇʀ( __filename, { name: 'Helpers.fromToken' } )

    const { map, isPair, has, get, typeOf } = ᴜɴɪᴛʏ

    if ( isPair( λ ) )
      return map( λ, ( v ) => ( Helpers.isToken( v ) ? v.token : v ) )

    // Used in final extraction

    if ( has( λ, 'token' ) )
      return get( λ, 'token' )

    throw alert(
      `Leaked value detected, Unexpected structure:  ${ typeOf( λ ) }`,
      SyntaxError
    )

  },
  toToken ( v ) {

    return { token: v }

  },
  isResource ( v ) {

    const { isArray, has } = ᴜɴɪᴛʏ

    return isArray( v ) && has( v, 'resource' )

  },

  fromResource ( v ) {

    const { map, identity, isArray } = ᴜɴɪᴛʏ

    if ( isArray( v ) )
      return map( v, identity )

    throw new Error( 'Resource: not array. No logic! ' )

  },

  isHelper ( v ) {

    const {
      isFunction,
      isString,
      isNumber,
      isRegExp,
      isArray,
      has,
      isPlainObject
    } = ᴜɴɪᴛʏ

    return (
      isFunction( v ) ||
      isString( v ) ||
      isNumber( v ) ||
      isRegExp( v ) ||
      isPlainObject( v ) ||
      isArray( v ) || // DETA KAN VARA DÅLIGT!!!
      Helpers.isResource( v )
    )

  }
}

// const isHelper = ( v ) => isFunction( v ) || isString( v ) || isNumber( v )

const engine = ( item ) => {

  let λ = item
  const { info, alert } = ʟᴏɢɢᴇʀ( __filename, engine )

  const {
    map,
    isArray,
    zip,
    isEqual,
    isFunction,
    inarmPair,
    typeOf,
    not,
    head,
    last,
    get,
    size
  } = ᴜɴɪᴛʏ

  const {
    asToken,
    fromToken,
    isHelper,
    isToken,
    toToken,
    isResource,
    fromResource
  } = Helpers

  if ( not( isArray( λ ) ) )
    throw alert( `Argument must be an Array, found ${ alert }`, SyntaxError )

  const test = map( λ, ( r ) => {

    if ( not( isArray( r ) ) )
      return true

    if ( isResource( r ) )
      return true

    return isEqual( size( r ), 2 )

  } )

  if ( not( isEqual( test, [ true, true ] ) ) )
    throw alert(
      `Argument must be an Pair of two(2) elements, found element with ${ size(
        test
      ) } elements`,
      SyntaxError
    )

  // eslint-disable-next-line no-param-reassign
  λ = map( λ, ( α ) => {

    if ( !isArray( α ) )
      return α

    if ( isResource( α ) )
      return α

    return engine( α )

  } )

  if ( isEqual( map( zip( [ isFunction, isToken ], λ ), inarmPair ), [ true, true ] ) ) {

    if ( false ) {

      console.log( '✳ Conv. [ Function, Token ] into an Token.' )
      console.log( pretty( λ ) )

    }

    return asToken( λ )

  }

  if ( isEqual( map( zip( [ isToken, isHelper ], λ ), inarmPair ), [ true, true ] ) ) {

    if ( false ) {

      console.log( '✳ Group [ Token, Helper ] as Token.' )
      console.log( pretty( λ ) )

    }

    return toToken( fromToken( λ ) )

  }

  if ( isEqual( map( zip( [ isFunction, isResource ], λ ), inarmPair ), [ true, true ] ) ) {

    const [ α, β ] = λ

    if ( false ) {

      console.log( '✳ Exc. [ Function, Helper ] execute.' )
      console.log( pretty( λ ) )

    }

    return α( fromResource( β ) )

  }

  if ( isEqual( map( zip( [ isFunction, isHelper ], λ ), inarmPair ), [ true, true ] ) ) {

    const [ α, β ] = λ

    if ( false ) {

      console.log( '✳ Exc. [ Function, Helper ] execute.' )
      console.log( pretty( λ ) )

    }

    return α( β )

  }

  throw alert(
    `Fatal: Leaked value detected, Unexpected structure:  ${ map( λ, typeOf ) }`,
    SyntaxError
  )

}

const ᴜɴɪ = ( worker ) => {

  const { partial } = ᴜɴɪᴛʏ

  const assets = {
    sʀᴄ ( val ) {

      const { toToken } = Helpers

      return toToken( val )

    },
    ᴘᴜᴛ ( val ) {

      const { alert } = ʟᴏɢɢᴇʀ( __filename, { name: 'ᴜɴɪ.ᴘᴜᴛ' } )

      const { not, isString, isNumber, isRegExp, merge, isArray } = ᴜɴɪᴛʏ

      // if ( not( isString( val ) || isNumber( val ) || isRegExp( val ) ) )
      //   throw alert(
      //     `Expected type of String or Number, found ${ typeof val }`,
      //     TypeError
      //   )

      if ( isArray( val ) )
        return merge( val, { resource: true } )

      return val

    },
    ᴜɴɪᴛʏ
  }

  const awaitsArgs: any = partial( worker, assets )

  // ʀᴜɴɴᴇʀ

  const main = ( ...args ) => {

    const {
      not,
      isNil,
      isEmpty,
      has,
      map,
      normalize,
      hasSome,
      isTrue,
      merge
    } = ᴜɴɪᴛʏ

    // const deb = hasSome(
    //   map( args, ( arg ) => has( arg, 'debug' ) && isTrue( arg.debug ) ),
    //   true
    // )

    const { alert } = ʟᴏɢɢᴇʀ( __filename, { name: 'ᴜɴɪ.runner' } )

    const { isToken, fromToken } = Helpers

    const inst = engine( awaitsArgs( args ) )

    if ( not( isToken( inst ) ) )
      throw alert( 'Expected a token to be produced', RangeError )

    const data = fromToken( inst )

    if ( isEmpty( data ) || isNil( data ) )
      throw alert( 'Expected product to contain data', TypeError )

    return data // normalize( data )

  }

  return main

}

export const ᴇxᴀᴍᴘʟᴇ = ᴘᴀʀᴛɪᴀʟ( ( unity, ruleset ) => {

  const paths = ᴜɴɪ( ( ᴀssᴇᴛs, [ sour, expr ] ) => {

    const { map, toPairs, last, leam, at } = ᴀssᴇᴛs.ᴜɴɪᴛʏ,
          { sʀᴄ, ᴘᴜᴛ } = ᴀssᴇᴛs

    return [ map, [[ map, [[ toPairs, sʀᴄ( sour ) ], last ]], [[ leam, at ], ᴘᴜᴛ( expr ) ]]]

  } )( ruleset, 'path.express' )

  return paths

}, ᴜɴɪᴛʏ )

export default ᴜɴɪ
