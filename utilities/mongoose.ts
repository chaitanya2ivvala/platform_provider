import chalk from 'chalk'
import dotenv from 'dotenv'
import mongoose from 'mongoose'

dotenv.load( { path: '.env.example' } )

/**
 * Connect to MongoDB.
 */
mongoose.set( 'useFindAndModify', false )
mongoose.set( 'useCreateIndex', true )
mongoose.set( 'useNewUrlParser', true )
mongoose.connect( process.env.MONGODB_URI )
mongoose.connection.on( 'error', ( err ) => {

  console.error( err )
  console.log(
    '%s MongoDB connection error. Please make sure MongoDB is running.',
    chalk.red( '✗' )
  )

  // process.exit()

  throw new Error(
    'MongoDB connection error. Please make sure MongoDB is running.'
  )

} )

console.log( 'Moongoose online' )
