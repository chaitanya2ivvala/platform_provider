import ConnectMongo from 'connect-mongo'
import chalk from 'chalk'
import dotenv from 'dotenv'
import expressValidator from 'express-validator'
import mongoose from 'mongoose'
import session from 'express-session'
import ʙᴏᴅʏPᴀʀsᴇʀ from 'body-parser'
import ᴇxᴘʀᴇss from 'express'

export default () => {

  dotenv.load( { path: '.env.example' } )

  const ᴀᴘᴘ = ᴇxᴘʀᴇss()

  const MongoStore = ConnectMongo( session )

  /**
   * Connect to MongoDB.
   */
  mongoose.set( 'useFindAndModify', false )
  mongoose.set( 'useCreateIndex', true )
  mongoose.set( 'useNewUrlParser', true )
  mongoose.connect( process.env.MONGODB_URI )
  mongoose.connection.on( 'error', ( err ) => {

    console.error( err )
    console.log(
      '%s MongoDB connection error. Please make sure MongoDB is running.',
      chalk.red( '✗' )
    )

    // process.exit()

    throw new Error(
      'MongoDB connection error. Please make sure MongoDB is running.'
    )

  } )

  /**
   * Load environment variables from .env file, where API keys and passwords are configured.
   */

  ᴀᴘᴘ.use( ʙᴏᴅʏPᴀʀsᴇʀ.json() )
  ᴀᴘᴘ.use( ʙᴏᴅʏPᴀʀsᴇʀ.urlencoded( { extended: false } ) )

  ᴀᴘᴘ.use( expressValidator() )
  ᴀᴘᴘ.use(
    session( {
      resave           : true,
      saveUninitialized: true,
      secret           : process.env.SESSION_SECRET,
      cookie           : { maxAge: 1209600000 }, // two weeks in milliseconds
      store            : new MongoStore( {
        url          : process.env.MONGODB_URI,
        autoReconnect: true
      } )
    } )
  )

  return ᴀᴘᴘ

}
