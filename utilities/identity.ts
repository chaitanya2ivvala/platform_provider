import ᴜɴɪǫ from 'uniqid'

//
// Assets
//

import ᴜɴɪᴛʏ from '../unity'

//
//
//

const { ᴘᴀʀᴛɪᴀʟ } = ᴜɴɪᴛʏ.ɢʟᴏʙᴀʟ

const ᴀssᴇᴍʙʟᴇʀ = ᴘᴀʀᴛɪᴀʟ( ( unity ) => {

  return {
    new () {

      return ᴜɴɪǫ.process()

    },
    short () {

      return ᴜɴɪǫ.process()

    },
    attach (
      type,
      container = { relationships: [], attributes: undefined },
      id = ᴜɴɪǫ.process()
    ) {

      const { merge, not, values, isUndefined } = ᴜɴɪᴛʏ

      const data = { id, type }

      const { attributes } = container

      if ( not( isUndefined( attributes ) ) )
        merge( data, { attributes } )

      const relationships = [ data, ...container.relationships ]

      return { relationships }

    }
  }

}, ᴜɴɪᴛʏ )

export default ᴀssᴇᴍʙʟᴇʀ()
